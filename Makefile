# final binary files
EXECUTABLE = bin/ix

# compiler
#CC       = gcc-4.9
CC        = clang-3.5
CFLAGS    = -std=gnu99 -Wall -Wextra -pedantic -O3

# linker flags
LDFLAGS   = 

# ix sources
SRC = \
		$(wildcard src/*.c)

# object files			
OBJS = \
		$(SRC:.c=.o)

# rules
all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -rf `find src -name '*.o'` ix.tar.gz $(EXECUTABLE)

tarball:
	tar -zcvf ix.tar.gz *
