#pragma once
#ifndef IX_LIST_H
#define IX_LIST_H

#include "types.h"

#include <stdint.h>
#include <stddef.h>

#define IX_LIST_DEFAULT_CAPACITY              4096

#define ix_construct_list(type, list, flags)  ix_construct(ix_list, list, sizeof(type), flags)
#define ix_new_list(type, flags)              ix_new(ix_list, sizeof(type), flags)

// it's simpler than it looks like
#define ix_list_foreach(var, list)            for ((var)      = (void*)((list)->first);                                                 \
                                                   (uint8_t*)(var)  < (((list)->first) + ((((list)->size))     * (list)->object_size)); \
                                                   var++)

#define ix_list_reverse_foreach(var, list)    for ((var)  =    (void*)(((list)->first) + ((((list)->size) - 1) * (list)->object_size)); \
                                                   (uint8_t*)(var)  >= ((list)->first);                                                 \
                                                   var--)

typedef enum ix_tag_list_flags
{
  IXLF_NONE    = 0x00,
  IXLF_OBJECT  = 0x01,
  IXLF_POINTER = 0x02,
} ix_list_flags;

typedef struct ix_tag_list
{
  IX_OBJECT;

  uint8_t*  first;
  size_t    size;
  size_t    capacity;

  size_t    object_size;
  uint8_t   flags;
} ix_list;

ix_list*  ix_list_ctor        (ix_list* object, size_t object_size, uint8_t flags);
void      ix_list_dtor        (ix_object* object);

void      ix_list_add         (ix_list* object, const void* item);
void      ix_list_add_many    (ix_list* object, const void* items, size_t items_count);
void      ix_list_insert      (ix_list* object, size_t index, const void* item);
void      ix_list_insert_many (ix_list* object, size_t index, const void* items, size_t items_count);
void      ix_list_remove      (ix_list* object, size_t index);
bool      ix_list_is_empty    (ix_list* object);

void*     ix_list_top         (ix_list* object);
void      ix_list_pop         (ix_list* object);
void      ix_list_pop_copy    (ix_list* object, void* result);

void*     ix_list_get         (ix_list* object, size_t index);

#endif
