#include "semantic.h"

#include <string.h>

#ifdef IX_COMPILER_MSVC
# pragma region function prototypes
#endif

//////////////////////////////////////////////////////////////////////////
//
// function prototypes
//
//////////////////////////////////////////////////////////////////////////

static bool ix_semantic_symbol_exists       (ix_symbol_item_ptr_list* symbol_item_list, const char* symbol_name);

static void ix_semantic_add_identifier      (ix_semantic* object, ix_symbol_table* parent, ix_ast_identifier* identifier);
static void ix_semantic_analyze_declaration (ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* declaration);
static void ix_semantic_analyze_call        (ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* call);
static void ix_semantic_analyze_for         (ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* st_for);
static void ix_semantic_analyze_while(ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* st_while);
static void ix_semantic_analyze_do_while(ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* st_do_while);
static void ix_semantic_analyze_if          (ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* st_if);
static void ix_semantic_analyze_expression  (ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* expression);
static void ix_semantic_analyze_block       (ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* block);
static void ix_semantic_analyze_function    (ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* function);

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region helper functions implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// helper functions implementation
//
//////////////////////////////////////////////////////////////////////////

static bool ix_semantic_symbol_exists(ix_symbol_item_ptr_list* symbol_item_list, const char* symbol_name)
{
  size_t i;

  for (i = 0; i < symbol_item_list->size; i++)
  {
    ix_symbol_table* symbol_item = ix_list_get(symbol_item_list, i);

    if (!strcmp(symbol_item->name, symbol_name) || ix_semantic_symbol_exists(&symbol_item->children, symbol_name))
    {
      return true;
    }
  }

  return false;
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region private implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// private implementation
//
//////////////////////////////////////////////////////////////////////////

static void ix_semantic_add_identifier(ix_semantic* object, ix_symbol_table* parent, ix_ast_identifier* identifier)
{
  ix_symbol_table* variable_symbol = ix_new(ix_symbol_table, IXST_VARIABLE);

  IX_UNUSED(object);

  strcpy(variable_symbol->name, identifier->identifier_name);

  ix_symbol_table_set_parent(variable_symbol, parent);
}

static void ix_semantic_add_string(ix_semantic* object, ix_symbol_table* parent, const char* value)
{
  ix_symbol_table* string_symbol = ix_new(ix_symbol_table, IXST_STRING);

  IX_UNUSED(object);

  strcpy(string_symbol->name, value);
  ix_variable_char_array(&string_symbol->variable, value);
  ix_variable_set_name(&string_symbol->variable, value);

  ix_symbol_table_set_parent(string_symbol, parent);
}

static void ix_semantic_analyze_declaration(ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* declaration)
{
  ix_semantic_add_identifier(object, parent, declaration->type.statement.st_declaration.identifier);

  if (declaration->type.statement.st_declaration.expression)
  {
    ix_semantic_analyze_expression(object, parent, declaration->type.statement.st_declaration.expression);
  }
}

static void ix_semantic_analyze_call(ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* call)
{
  size_t i;

  if (!ix_semantic_symbol_exists(&object->symbol_table->children, call->type.statement.st_call.function_name->identifier_name))
  {
    ix_symbol_table* block = ix_new(ix_symbol_table, IXST_FUNCTION);

    strcpy(block->name, call->type.statement.st_call.function_name->identifier_name);

    ix_symbol_table_set_parent(block, parent);
  }

  for (i = 0; i < call->type.statement.st_call.argument_list->size; i++)
  {
    ix_ast_expression* argument = ix_list_get(call->type.statement.st_call.argument_list, i);
    ix_semantic_analyze_expression(object, parent, argument);
  }
}

static void ix_semantic_analyze_for(ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* st_for)
{
  ix_symbol_table* block = ix_new(ix_symbol_table, IXST_BLOCK);

  ix_symbol_table_set_parent(block, parent);

  ix_semantic_analyze_expression(object, block, st_for->type.statement.st_for.init);
  ix_semantic_analyze_expression(object, block, st_for->type.statement.st_for.condition);
  ix_semantic_analyze_expression(object, block, st_for->type.statement.st_for.count);

  ix_semantic_analyze_block(object, block, st_for->type.statement.st_for.block);
}

static void ix_semantic_analyze_while(ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* st_while)
{
  ix_symbol_table* block = ix_new(ix_symbol_table, IXST_BLOCK);

  ix_symbol_table_set_parent(block, parent);

  ix_semantic_analyze_expression(object, block, st_while->type.statement.st_while.condition);
  ix_semantic_analyze_block(object, block, st_while->type.statement.st_while.block);
}

static void ix_semantic_analyze_do_while(ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* st_do_while)
{
  ix_symbol_table* block = ix_new(ix_symbol_table, IXST_BLOCK);

  ix_symbol_table_set_parent(block, parent);

  ix_semantic_analyze_block(object, block, st_do_while->type.statement.st_do_while.block);
  ix_semantic_analyze_expression(object, block, st_do_while->type.statement.st_do_while.condition);
}

static void ix_semantic_analyze_if(ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* st_if)
{
  ix_symbol_table* block = ix_new(ix_symbol_table, IXST_BLOCK);

  ix_symbol_table_set_parent(block, parent);

  ix_semantic_analyze_expression(object, block, st_if->type.statement.st_if.condition);

  ix_semantic_analyze_block(object, block, st_if->type.statement.st_if.true_block);

  if (st_if->type.statement.st_if.false_block)
  {
    ix_semantic_analyze_block(object, block, st_if->type.statement.st_if.false_block);
  }
}

static void ix_semantic_analyze_expression(ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* expression)
{
  switch (ix_ast_get_type(expression))
  {
    case IXAT_VALUE_VARIABLE:
      ix_semantic_add_identifier(object, parent, expression->type.value.variable);
      break;

    case IXAT_VALUE_STRING:
      ix_semantic_add_string(object, parent, expression->type.value.string.content);
      break;

    case IXAT_STATEMENT_FOR:
      ix_semantic_analyze_for(object, parent, expression);
      break;

    case IXAT_STATEMENT_WHILE:
      ix_semantic_analyze_while(object, parent, expression);
      break;

    case IXAT_STATEMENT_DO_WHILE:
      ix_semantic_analyze_do_while(object, parent, expression);
      break;

    case IXAT_STATEMENT_IF:
      ix_semantic_analyze_if(object, parent, expression);
      break;

    case IXAT_STATEMENT_FUNCTION:
      ix_semantic_analyze_function(object, parent, expression);
      break;

    case IXAT_STATEMENT_CALL:
      ix_semantic_analyze_call(object, parent, expression);
      break;

    case IXAT_STATEMENT_DECLARATION:
      ix_semantic_analyze_declaration(object, parent, expression);
      break;

    case IXAT_EXPRESSION_UNARY:
      ix_semantic_analyze_expression(object, parent, expression->type.unary.expression);
      break;

    case IXAT_EXPRESSION_BINARY:
      ix_semantic_analyze_expression(object, parent, expression->type.binary.left);
      ix_semantic_analyze_expression(object, parent, expression->type.binary.right);
      break;

    default:
      // IX_ASSERT(0);
      break;
  }
}

static void ix_semantic_analyze_block(ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* block)
{
  size_t i;

  for (i = 0; i < block->type.statement.st_block.statement_list->size; i++)
  {
    ix_ast_expression* statement = ix_list_get(block->type.statement.st_block.statement_list, i);
    ix_semantic_analyze_expression(object, parent, statement);
  }
}

static void ix_semantic_analyze_function(ix_semantic* object, ix_symbol_table* parent, ix_ast_expression* function)
{
  size_t i;
  ix_symbol_table* block = ix_new(ix_symbol_table, IXST_FUNCTION);
  strcpy(block->name, function->type.statement.st_function.function_name->identifier_name);
  ix_symbol_table_set_parent(block, parent);

  for (i = 0; i < function->type.statement.st_function.parameter_list->size; i++)
  {
    ix_ast_identifier* identifier = ix_list_get(function->type.statement.st_function.parameter_list, i);
    ix_semantic_add_identifier(object, parent, identifier);
  }

  ix_semantic_analyze_block(object, block, function->type.statement.st_function.body);
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix_semantic* ix_semantic_ctor(ix_semantic* object, ix_error_list* errors)
{
  IX_OBJECT_SET_DTOR(object, &ix_semantic_dtor);

  object->errors = errors;

  return object;
}

void ix_semantic_dtor(ix_object* object)
{
  ix_semantic* semantic = (ix_semantic*)object;

  IX_UNUSED(semantic);
}

void ix_semantic_analyze(ix_semantic* object, ix_ast* ast, ix_symbol_table* symbol_table)
{
  size_t i;

  object->symbol_table = symbol_table;

  for (i = 0; i < ast->program->statement_list->size; i++)
  {
    ix_ast_expression* statement = ix_list_get(ast->program->statement_list, i);
    ix_semantic_analyze_block(object, symbol_table, statement);
  }
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
