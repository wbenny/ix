#pragma once

#include "ast.h"
#include "symbol.h"
#include "list.h"
#include "vm_context.h"

typedef ix_list ix_code;
typedef ix_list ix_variable_list;
typedef ix_list ix_jmp_table_map;

typedef struct ix_tag_function_offset_pair
{
  uint32_t          name_hash;
  ix_offset_type    offset;
} ix_function_offset_pair;

typedef struct ix_tag_context
{
  ix_object         base;

  ix_code           code;
  size_t            code_section;

  ix_variable_list  variable_list;
  ix_jmp_table_map  jmp_table;
  ix_jmp_table_map  jmp_table_reverse;

  ix_vm_context     vm_context;
} ix_context;

static ix_context* ix_context_ctor(ix_context* object)
{
  ix_construct_list(&object->code,              uint8_t,                  IXLF_NONE);
  ix_construct_list(&object->variable_list,     ix_variable,              IXLF_OBJECT);
  ix_construct_list(&object->jmp_table,         ix_function_offset_pair,  IXLF_NONE);
  ix_construct_list(&object->jmp_table_reverse, ix_function_offset_pair,  IXLF_NONE);
  ix_construct(ix_vm_context, &object->vm_context);

  return object;
}