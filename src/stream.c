#include "stream.h"

#include <stdint.h>

#ifdef IX_COMPILER_MSVC
# pragma region virtual calls
#endif

//////////////////////////////////////////////////////////////////////////
//
// virtual calls
//
//////////////////////////////////////////////////////////////////////////

size_t ix_stream_read(void* object, void* buffer, size_t size)
{
  return ((ix_stream*)object)->read(object, buffer, size);
}

size_t ix_stream_write(void* object, const void* buffer, size_t size)
{
  return ((ix_stream*)object)->write(object, buffer, size);
}

bool ix_stream_seek(void* object, ix_stream_origin origin, long offset)
{
  return ((ix_stream*)object)->seek(object, origin, offset);
}

size_t ix_stream_resize(void* object, size_t new_size)
{
  return ((ix_stream*)object)->resize(object, new_size);
}

size_t ix_stream_size(void* object)
{
  return ((ix_stream*)object)->size(object);
}

size_t ix_stream_position(void* object)
{
  return ((ix_stream*)object)->position(object);
}

void ix_stream_close(void* object)
{
  ((ix_stream*)object)->close(object);
}

size_t ix_stream_copy_from(void* object, void* source_stream)
{
  uint8_t buffer[4 * 1024];
  size_t bytes_read;
  size_t total_bytes_read = 0;

  do
  {
    bytes_read = ix_stream_read(source_stream, buffer, sizeof(buffer));
    ix_stream_write(object, buffer, sizeof(buffer));

    total_bytes_read += bytes_read;
  } while (bytes_read == sizeof(buffer));

  return total_bytes_read;
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
