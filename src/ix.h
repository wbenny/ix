#pragma once
#ifndef IX_IX_H
#define IX_IX_H

#include "stream.h"
#include "vm.h"
#include "error.h"
#include "types.h"

typedef struct ix_tag
{
  IX_OBJECT;

  ix_error_list errors;
  ix_vm         vm;
} ix;

ix*  ix_ctor              (ix* object);
void ix_dtor              (ix_object* object);

void ix_process_stream    (ix* object, ix_stream* stream);
void ix_register_function (ix* object, const char* function_name, ix_extern_callback* callback);
void ix_run               (ix* object);

#endif
