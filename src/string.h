#pragma once
#ifndef IX_STRING_H
#define IX_STRING_H

#include "types.h"

#include <stdint.h>

#define IX_STRING_DEFAULT_CAPACITY        4096

typedef struct ix_tag_string
{
  IX_OBJECT;

  size_t  size;
  size_t  capacity;
  char*   content;
} ix_string;

ix_string*  ix_string_ctor          (ix_string* object);
void        ix_string_dtor          (ix_object* object);
void        ix_string_copy_from     (ix_string* object, ix_string* source);

void        ix_string_reserve       (ix_string* object, size_t size);

void        ix_string_append_array  (ix_string* object, const char* string, size_t size);
void        ix_string_set_array     (ix_string* object, const char* string, size_t size);

void        ix_string_append        (ix_string* object, const char* string);
void        ix_string_append_char   (ix_string* object, char c);
void        ix_string_append_string (ix_string* object, ix_string* string);

void        ix_string_set           (ix_string* object, const char* string);
void        ix_string_set_char      (ix_string* object, char c);
void        ix_string_set_string    (ix_string* object, ix_string* string);

void        ix_string_substring     (ix_string* object, ix_string* dest, size_t index, size_t count);

uint32_t    ix_string_hash          (const char* string);

#endif
