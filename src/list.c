#include "list.h"

#include <stdlib.h>
#include <memory.h>

#ifdef IX_COMPILER_MSVC
# pragma region function prototypes
#endif

//////////////////////////////////////////////////////////////////////////
//
// function prototypes
//
//////////////////////////////////////////////////////////////////////////

static void   ix_list_insert_impl (ix_list* object, size_t index, const void* item);
static void*  ix_list_get_impl    (ix_list* object, size_t index);

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region private implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// private implementation
//
//////////////////////////////////////////////////////////////////////////

static void ix_list_insert_impl(ix_list* object, size_t index, const void* item)
{
  while (index >= object->capacity)
  {
    object->first = realloc(
      object->first,
      object->object_size * (object->capacity <<= 2)
      );
  }

  memcpy(
    (object->first + (index * object->object_size)),
    item,
    object->object_size
  );

  if (index + 1 > object->size)
  {
    object->size = index + 1;
  }
}

static void* ix_list_get_impl(ix_list* object, size_t index)
{
  return (object->first + (index * object->object_size));
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix_list* ix_list_ctor(ix_list* object, size_t object_size, uint8_t flags)
{
  IX_OBJECT_SET_DTOR(object, &ix_list_dtor);

  object->first = ix_alloc_byte_array(object_size * IX_LIST_DEFAULT_CAPACITY);
  object->size = 0;
  object->capacity = IX_LIST_DEFAULT_CAPACITY;

  object->object_size = object_size;
  object->flags = flags;

  return object;
}

void ix_list_dtor(ix_object* object)
{
  ix_list* list = (ix_list*)object;

  if (list->flags & IXLF_OBJECT)
  {
    size_t i;

    for (i = 0; i < list->size; i++)
    {
      if (list->flags & IXLF_POINTER)
      {
        ix_delete(ix_list_get(list, i));
      }
      else
      {
        ix_destroy(ix_list_get(list, i));
      }
    }
  }

  ix_free(list->first);
}

void ix_list_add(ix_list* object, const void* item)
{
  const void* ptr_to_add = (object->flags & IXLF_POINTER)
    ? &item
    : item;

  ix_list_insert_impl(object, object->size, ptr_to_add);
}

void ix_list_add_many(ix_list* object, const void* items, size_t items_count)
{
  size_t i;

  for (i = 0; i < items_count; i++)
  {
    ix_list_add(object, (uint8_t*)items + (i * object->object_size));
  }
}

void ix_list_insert(ix_list* object, size_t index, const void* item)
{
  const void* ptr_to_add = (object->flags & IXLF_POINTER)
    ? &item
    : item;

  ix_list_insert_impl(object, index, ptr_to_add);
}

void ix_list_insert_many(ix_list* object, size_t index, const void* items, size_t items_count)
{
  size_t i;

  for (i = 0; i < items_count; i++)
  {
    ix_list_insert(object, index++, (uint8_t*)items + (i * object->object_size));
  }
}

void ix_list_remove(ix_list* object, size_t index)
{
  if (index != (object->size - 1))
  {
    memcpy(
      (object->first  + (index       * object->object_size)),
      (object->first  + ((index + 1) * object->object_size)),
      (object->size   - index)       * object->object_size
    );
  }

  object->size--;
}

bool ix_list_is_empty(ix_list* object)
{
  return object->size == 0;
}

void* ix_list_top(ix_list* object)
{
  return ix_list_get(object, object->size - 1);
}

void ix_list_pop(ix_list* object)
{
  ix_list_remove(object, object->size - 1);
}

void ix_list_pop_copy(ix_list* object, void* result)
{
  void* top = ix_list_top(object);
  void* value_ptr = (object->flags & IXLF_POINTER)
    ? &top
    : top;

  memcpy(result, value_ptr, object->object_size);
  ix_list_pop(object);
}

void* ix_list_get(ix_list* object, size_t index)
{
  return (object->flags & IXLF_POINTER)
    ? *(void**)ix_list_get_impl(object, index)
    :          ix_list_get_impl(object, index);
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
