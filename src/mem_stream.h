#pragma once
#ifndef IX_MEM_STREAM_H
#define IX_MEM_STREAM_H

#include "stream.h"
#include "list.h"

#include <stdint.h>

typedef struct ix_tag_mem_stream
{
  ix_stream base;

  uint8_t* (*data)(void* object);

  ix_list buffer;
  size_t  position;
} ix_mem_stream;

ix_mem_stream*  ix_mem_stream_ctor(ix_mem_stream* object);
void            ix_mem_stream_dtor(ix_object* object);

uint8_t*        ix_mem_stream_data(void* object);

#endif
