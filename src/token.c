#include "token.h"

#ifdef IX_COMPILER_MSVC
# pragma region tables
#endif

//////////////////////////////////////////////////////////////////////////
//
// tables
//
//////////////////////////////////////////////////////////////////////////

ix_token_state_item ix_token_state_table[] =
{
  { IXT_NONE,             '+', IXT_PLUS,                    IXT_PLUS            },
  { IXT_PLUS,             '=', IXT_PLUS_ASSIGN,             IXT_NONE            },
  { IXT_PLUS,             '+', IXT_INCREMENT,               IXT_NONE            },

  { IXT_NONE,             '-', IXT_MINUS,                   IXT_MINUS           },
  { IXT_MINUS,            '=', IXT_MINUS_ASSIGN,            IXT_NONE            },
  { IXT_MINUS,            '-', IXT_DECREMENT,               IXT_NONE            },

  { IXT_NONE,             '*', IXT_MULTIPLY,                IXT_MULTIPLY        },
  { IXT_MULTIPLY,         '=', IXT_MULTIPLY_ASSIGN,         IXT_NONE            },

  { IXT_NONE,             '/', IXT_DIVIDE,                  IXT_DIVIDE          },
  { IXT_DIVIDE,           '=', IXT_DIVIDE_ASSIGN,           IXT_NONE            },

  { IXT_NONE,             '%', IXT_MODULO,                  IXT_MODULO          },
  { IXT_MODULO,           '=', IXT_MODULO_ASSIGN,           IXT_NONE            },

  { IXT_NONE,             '=', IXT_ASSIGN,                  IXT_ASSIGN          },
  { IXT_ASSIGN,           '=', IXT_EQUAL,                   IXT_NONE            },

  { IXT_NONE,             '!', IXT_NOT,                     IXT_NONE            },
  { IXT_NOT,              '=', IXT_NOT_EQUAL,               IXT_NONE            },

  { IXT_NONE,             '<', IXT_LESS_THAN,               IXT_LESS_THAN       },
  { IXT_LESS_THAN,        '=', IXT_LESS_THAN_OR_EQUAL,      IXT_NONE            },
  { IXT_LESS_THAN,        '<', IXT_BIT_SHIFT_LEFT,          IXT_BIT_SHIFT_LEFT  },
  { IXT_BIT_SHIFT_LEFT,   '=', IXT_BIT_SHIFT_LEFT_ASSIGN,   IXT_NONE            },

  { IXT_NONE,             '>', IXT_GREATER_THAN,            IXT_GREATER_THAN    },
  { IXT_GREATER_THAN,     '=', IXT_LESS_THAN_OR_EQUAL,      IXT_NONE            },
  { IXT_GREATER_THAN,     '>', IXT_BIT_SHIFT_RIGHT,         IXT_BIT_SHIFT_RIGHT },
  { IXT_BIT_SHIFT_RIGHT,  '=', IXT_BIT_SHIFT_RIGHT_ASSIGN,  IXT_NONE            },

  { IXT_NONE,             '~', IXT_BITWISE_NOT,             IXT_NONE            },

  { IXT_NONE,             '&', IXT_BITWISE_AND,             IXT_BITWISE_AND     },
  { IXT_BITWISE_AND,      '=', IXT_BITWISE_AND_ASSIGN,      IXT_NONE            },

  { IXT_NONE,             '|', IXT_BITWISE_OR,              IXT_BITWISE_OR      },
  { IXT_BITWISE_OR,       '=', IXT_BITWISE_OR_ASSIGN,       IXT_NONE            },

  { IXT_NONE,             '^', IXT_BITWISE_XOR,             IXT_BITWISE_XOR     },
  { IXT_BITWISE_XOR,      '=', IXT_BITWISE_XOR_ASSIGN,      IXT_NONE            },

  { IXT_NONE,             '(', IXT_LEFT_PARENTHESIS,        IXT_NONE            },
  { IXT_NONE,             ')', IXT_RIGHT_PARENTHESIS,       IXT_NONE            },
  { IXT_NONE,             '[', IXT_LEFT_BRACKET,            IXT_NONE            },
  { IXT_NONE,             ']', IXT_RIGHT_BRACKET,           IXT_NONE            },
  { IXT_NONE,             '{', IXT_LEFT_BRACE,              IXT_NONE            },
  { IXT_NONE,             '}', IXT_RIGHT_BRACE,             IXT_NONE            },

  { IXT_NONE,             ',', IXT_COMMA,                   IXT_NONE            },
  { IXT_NONE,             ';', IXT_SEMICOLON,               IXT_NONE            },
};

ix_token_keyword_item ix_token_keyword_table[] =
{
  { IXT_KW_FUNCTION,  "function"  },
  { IXT_KW_VAR,       "var"       },
  { IXT_KW_NULL,      "null"      },
  { IXT_KW_TRUE,      "true"      },
  { IXT_KW_FALSE,     "false"     },
  { IXT_KW_IF,        "if"        },
  { IXT_KW_ELSE,      "else"      },
  { IXT_KW_FOR,       "for"       },
  { IXT_KW_DO,        "do"        },
  { IXT_KW_WHILE,     "while"     },
  { IXT_KW_SWITCH,    "switch"    },
  { IXT_KW_CASE,      "case"      },
  { IXT_KW_CONTINUE,  "continue"  },
  { IXT_KW_BREAK,     "break"     },
  { IXT_KW_RETURN,    "return"    },
};

const size_t ix_token_state_table_size   = ix_count_of(ix_token_state_table);
const size_t ix_token_keyword_table_size = ix_count_of(ix_token_keyword_table);

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
