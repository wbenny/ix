#include "emitter.h"

#include <string.h>

#ifdef IX_COMPILER_MSVC
# pragma region private structs
#endif

//////////////////////////////////////////////////////////////////////////
//
// private structs
//
//////////////////////////////////////////////////////////////////////////

typedef struct ix_tag_function_offset_pair
{
  uint32_t       name_hash;
  ix_offset_type offset;
} ix_function_offset_pair;

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region function prototypes
#endif

//////////////////////////////////////////////////////////////////////////
//
// function prototypes
//
//////////////////////////////////////////////////////////////////////////

static ix_variable*             ix_emitter_find_variable(ix_emitter* object, const char* name);

static ix_function_offset_pair* ix_jmp_table_map_find   (ix_jmp_table_map* jmp_table, uint32_t name_hash);

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region private implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// private implementation
//
//////////////////////////////////////////////////////////////////////////

static ix_variable* ix_emitter_find_variable(ix_emitter* object, const char* name)
{
  size_t i;

  uint32_t name_hash = ix_string_hash(name);

  for (i = 0; i < object->variable_list.size; i++)
  {
    ix_variable* variable = ix_list_get(&object->variable_list, i);
    
    if (variable->name_hash == name_hash)
    {
      return variable;
    }
  }

  return NULL;
}

static ix_function_offset_pair* ix_jmp_table_map_find(ix_jmp_table_map* jmp_table, uint32_t name_hash)
{
  size_t i;

  for (i = 0; i < jmp_table->size; i++)
  {
    ix_function_offset_pair* fn_off_item = ix_list_get(jmp_table, i);

    if (name_hash == fn_off_item->name_hash)
    {
      return fn_off_item;
    }
  }

  return NULL;
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix_emitter* ix_emitter_ctor(ix_emitter* object, ix_stream* code_stream)
{
  IX_OBJECT_SET_DTOR(object, &ix_emitter_dtor);

  ix_construct_list(ix_variable,              &object->variable_list,     IXLF_OBJECT);
  ix_construct_list(ix_function_offset_pair,  &object->jmp_table,         IXLF_NONE);
  ix_construct_list(ix_function_offset_pair,  &object->jmp_table_reverse, IXLF_NONE);
  
  object->code_stream = code_stream;
  object->code_section = 0;

  return object;
}

void ix_emitter_dtor(ix_object* object)
{
  ix_emitter* emitter = (ix_emitter*)object;

  ix_destroy(&emitter->jmp_table_reverse);
  ix_destroy(&emitter->jmp_table);
  ix_destroy(&emitter->variable_list);
}

void ix_emitter_emit(ix_emitter* object, ix_op opcode, ix_vm_argument_type arg_type, const void* arg)
{
  ix_stream_write(object->code_stream, &opcode, 1);

  switch (arg_type)
  {
    default:
    case IXVMAT_NONE:
      break;

    case IXVMAT_INTEGER:
      ix_stream_write(object->code_stream, arg, sizeof(ix_integer_type));
      break;

    case IXVMAT_REAL:
      ix_stream_write(object->code_stream, arg, sizeof(ix_real_type));
      break;

    case IXVMAT_LABEL:
      {
        const char* identifier_name = arg;

        uintptr_t list_begin   = (uintptr_t)object->variable_list.first;
        uintptr_t variable_idx = (uintptr_t)ix_emitter_find_variable(object, identifier_name);

        ix_offset_type offset = (ix_offset_type)((variable_idx - list_begin) / object->variable_list.object_size);

        ix_stream_write(object->code_stream, &offset, sizeof(ix_offset_type));
      }
      break;

    case IXVMAT_JUMP:
      {
        const char* identifier_name = arg;
        ix_function_offset_pair fn_off_item;
        ix_offset_type dummy_offset = 0;

        fn_off_item.name_hash = ix_string_hash(identifier_name);
        fn_off_item.offset    = (ix_offset_type)ix_stream_size(object->code_stream);
        ix_list_add(&object->jmp_table_reverse, &fn_off_item);

        ix_stream_write(object->code_stream, &dummy_offset, sizeof(ix_offset_type));
      }
      break;
  }
}

void ix_emitter_emit_label(ix_emitter* object, const char* label)
{
  ix_function_offset_pair fn_off_item;

  fn_off_item.name_hash = ix_string_hash(label);
  fn_off_item.offset    = (ix_offset_type)(ix_stream_size(object->code_stream) - object->code_section);
  ix_list_add(&object->jmp_table, &fn_off_item);
}

void ix_emitter_emit_variable(ix_emitter* object, ix_variable* variable)
{
  // [nameSize{1}][name{nameSize}][type{1}][value{0|4|8|str}]

  ix_value* value = &variable->value;
  
  if (!ix_emitter_find_variable(object, variable->name))
  {
    uint8_t name_size = (uint8_t)strlen(variable->name);

    ix_stream_write(object->code_stream, &name_size, sizeof(name_size));
    ix_stream_write(object->code_stream, variable->name, name_size);
    ix_stream_write(object->code_stream, &value->type, sizeof(uint8_t));

    switch (value->type)
    {
      uint32_t string_size;
      ix_function_offset_pair fn_off_item;

      case IXVT_NULL:

        //
        // write nothing
        //

        break;

      case IXVT_FUNCTION:

        //
        // add item to reverse jmp table
        //

        fn_off_item.name_hash = variable->name_hash;
        fn_off_item.offset    = (ix_offset_type)ix_stream_size(object->code_stream);
        ix_list_add(&object->jmp_table_reverse, &fn_off_item);

        ix_stream_write(object->code_stream, &value->value.offset, sizeof(value->value.offset));
        break;

      case IXVT_INTEGER:
        ix_stream_write(object->code_stream, &value->value.integer, sizeof(value->value.integer));
        break;

      case IXVT_REAL:
        ix_stream_write(object->code_stream, &value->value.real, sizeof(value->value.real));
        break;

      case IXVT_STRING:
        string_size = (uint32_t)value->value.string.size;
        ix_stream_write(object->code_stream, &string_size, sizeof(string_size));
        ix_stream_write(object->code_stream, value->value.string.content, string_size);
        break;

      default:
        IX_ASSERT(0);
        break;
    }

    ix_list_add(&object->variable_list, variable);
  }
}

void ix_emitter_mark_end_of_data_section(ix_emitter* object)
{
  uint8_t end_point = 0;

  ix_stream_write(object->code_stream, &end_point, sizeof(end_point));
  object->code_section = ix_stream_size(object->code_stream);
}

void ix_emitter_fix_jumps(ix_emitter* object)
{
  size_t i;

  for (i = 0; i < object->jmp_table_reverse.size; i++)
  {
    ix_function_offset_pair* fn_off_item = ix_list_get(&object->jmp_table_reverse, i);
    ix_function_offset_pair* fn_off_complement = ix_jmp_table_map_find(&object->jmp_table, fn_off_item->name_hash);
    ix_offset_type address;
    
    address = fn_off_complement
      ? fn_off_complement->offset
      : -1; // extern call;

    ix_stream_seek(object->code_stream, IXSS_SET, fn_off_item->offset);
    ix_stream_write(object->code_stream, &address, sizeof(address));
  }
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
