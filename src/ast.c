#include "ast.h"

#include <stdio.h>
#include <inttypes.h>

#ifdef IX_COMPILER_MSVC
# pragma region function prototypes
#endif

//////////////////////////////////////////////////////////////////////////
//
// function prototypes
//
//////////////////////////////////////////////////////////////////////////

static void ix_ast_identifier_ptr_list_dump (ix_ast_identifier_list* object,      int indent);
static void ix_ast_expression_ptr_list_dump (ix_ast_expression_ptr_list* object,  int indent);
static void ix_ast_expression_dump          (ix_ast_expression* object,           int indent);

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region private implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// private implementation
//
//////////////////////////////////////////////////////////////////////////

#define IX_AST_PRINT0(str)              printf("%*s" str "\n", indent, "");
#define IX_AST_PRINT1(str, ...)         printf("%*s" str "\n", indent, "", __VA_ARGS__);
#define IX_AST_PRINT_NUM_ARGS_TABLE     1, 1, 1, 0, 0
#define ix_ast_print(...)               IX_VA_FUNCTION(IX_AST_PRINT, __VA_ARGS__)

static void ix_ast_identifier_ptr_list_dump(ix_ast_identifier_list* object, int indent)
{
  size_t i;

  for (i = 0; i < object->size; i++)
  {
    ix_ast_print("'%s'", ((ix_ast_identifier*)ix_list_get(object, i))->identifier_name);
  }
}

static void ix_ast_expression_ptr_list_dump(ix_ast_expression_ptr_list* object, int indent)
{
  size_t i;

  for (i = 0; i < object->size; i++)
  {
    ix_ast_expression_dump(ix_list_get(object, i), indent);
  }
}

static void ix_ast_expression_dump(ix_ast_expression* object, int indent)
{
  if (!object) return;

  switch (object->base.type)
  {
    case IXAT_EXPRESSION_UNARY:
      ix_ast_print("IXAT_EXPRESSION_UNARY:");
      ix_ast_print(".expression:");
      ix_ast_expression_dump(object->type.unary.expression, indent + 2);
      ix_ast_print(".operation: %i", object->type.unary.operation);
      break;

    case IXAT_EXPRESSION_BINARY:
      ix_ast_print("IXAT_EXPRESSION_BINARY:");
      ix_ast_print(".left:");
      ix_ast_expression_dump(object->type.binary.left, indent + 2);
      ix_ast_print(".right:");
      ix_ast_expression_dump(object->type.binary.right, indent + 2);
      ix_ast_print(".operation: %i", object->type.binary.operation);
      break;

    case IXAT_VALUE_CONSTANT:

    case IXAT_VALUE_VARIABLE:
      ix_ast_print("IXAT_VALUE_VARIABLE:");
      ix_ast_print(".variable: '%s'", object->type.value.variable->identifier_name);
      break;

    case IXAT_VALUE_STRING:
      ix_ast_print("IXAT_VALUE_STRING:");
      ix_ast_print(".string: '%s'", object->type.value.string.content);
      break;

    case IXAT_VALUE_INTEGER:
      ix_ast_print("IXAT_VALUE_INTEGER:");
      ix_ast_print(".integer: %" PRId64, object->type.value.integer);
      break;

    case IXAT_VALUE_REAL:
      ix_ast_print("IXAT_VALUE_REAL:");
      ix_ast_print(".integer: %f", object->type.value.real);
      break;

    case IXAT_STATEMENT_BLOCK:
      ix_ast_print("IXAT_STATEMENT_BLOCK:");
      ix_ast_print(".statement_list:");
      ix_ast_expression_ptr_list_dump(object->type.statement.st_block.statement_list, indent + 2);
      break;

    case IXAT_STATEMENT_FOR:
      ix_ast_print("IXAT_STATEMENT_FOR:");
      ix_ast_print(".init:");
      ix_ast_expression_dump(object->type.statement.st_for.init, indent + 2);
      ix_ast_print(".condition:");
      ix_ast_expression_dump(object->type.statement.st_for.condition, indent + 2);
      ix_ast_print(".count:");
      ix_ast_expression_dump(object->type.statement.st_for.count, indent + 2);
      ix_ast_print(".block:");
      ix_ast_expression_dump(object->type.statement.st_for.block, indent + 2);
      break;

    case IXAT_STATEMENT_WHILE:
      ix_ast_print("IXAT_STATEMENT_WHILE:");
      ix_ast_print(".condition:");
      ix_ast_expression_dump(object->type.statement.st_while.condition, indent + 2);
      ix_ast_print(".block:");
      ix_ast_expression_dump(object->type.statement.st_while.block, indent + 2);
      break;

    case IXAT_STATEMENT_DO_WHILE:
      ix_ast_print("IXAT_STATEMENT_DO_WHILE:");
      ix_ast_print(".block:");
      ix_ast_expression_dump(object->type.statement.st_do_while.block, indent + 2);
      ix_ast_print(".condition:");
      ix_ast_expression_dump(object->type.statement.st_do_while.condition, indent + 2);
      break;

    case IXAT_STATEMENT_IF:
      ix_ast_print("IXAT_STATEMENT_IF:");
      ix_ast_print(".condition:");
      ix_ast_expression_dump(object->type.statement.st_if.condition, indent + 2);
      ix_ast_print(".true_block:");
      ix_ast_expression_dump(object->type.statement.st_if.true_block, indent + 2);
      
      if (object->type.statement.st_if.false_block)
      {
        ix_ast_print(".false_block:");
        ix_ast_expression_dump(object->type.statement.st_if.false_block, indent + 2);
      }
      break;

    case IXAT_STATEMENT_LAST:
      ix_ast_print("IXAT_STATEMENT_LAST:");
      ix_ast_print(".keyword: %d", object->type.statement.st_last.keyword);
      ix_ast_print(".expression:");
      ix_ast_expression_dump(object->type.statement.st_last.expression, indent + 2);
      break;

    case IXAT_STATEMENT_FUNCTION:
      ix_ast_print("IXAT_STATEMENT_FUNCTION:");
      ix_ast_print(".function_name: '%s'", object->type.statement.st_function.function_name->identifier_name);
      ix_ast_print(".parameter_list:");
      ix_ast_identifier_ptr_list_dump(object->type.statement.st_function.parameter_list, indent + 2);
      ix_ast_print(".body:");
      ix_ast_expression_dump(object->type.statement.st_function.body, indent + 2);
      break;

    case IXAT_STATEMENT_CALL:
      ix_ast_print("IXAT_STATEMENT_CALL:");
      ix_ast_print(".function_name: '%s'", object->type.statement.st_call.function_name->identifier_name);
      ix_ast_print(".argument_list:");
      ix_ast_expression_ptr_list_dump(object->type.statement.st_function.parameter_list, indent + 2);
      break;

    case IXAT_STATEMENT_DECLARATION:
      ix_ast_print("IXAT_STATEMENT_DECLARATION:");
      ix_ast_print(".identifier: '%s'", object->type.statement.st_declaration.identifier->identifier_name);
      ix_ast_print(".expression:");
      ix_ast_expression_dump(object->type.statement.st_declaration.expression, indent + 2);
      break;

    default:
      IX_ASSERT(0);
      break;
  }
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix_ast_identifier* ix_ast_identifier_ctor(ix_ast_identifier* object)
{
  IX_OBJECT_SET_DTOR(object, &ix_ast_identifier_dtor);

  return object;
}

void ix_ast_identifier_dtor(ix_object* object)
{
  ix_ast_identifier* identifier = (ix_ast_identifier*)object;

  IX_UNUSED(identifier);
}

ix_ast_expression* ix_ast_expression_ctor(ix_ast_expression* object)
{
  IX_OBJECT_SET_DTOR(object, &ix_ast_expression_dtor);

  return object;
}

void ix_ast_expression_dtor(ix_object* object)
{
  ix_ast_expression* expression = (ix_ast_expression*)object;

  switch (ix_ast_get_type(expression))
  {
    case IXAT_EXPRESSION_UNARY:
      ix_delete(expression->type.unary.expression);
      break;

    case IXAT_EXPRESSION_BINARY:
      ix_delete(expression->type.binary.left);
      ix_delete(expression->type.binary.right);
      break;
    
    case IXAT_STATEMENT_BLOCK:
      ix_delete(expression->type.statement.st_block.statement_list);
      break;
    
    case IXAT_STATEMENT_FOR:
      ix_delete(expression->type.statement.st_for.init);
      ix_delete(expression->type.statement.st_for.condition);
      ix_delete(expression->type.statement.st_for.count);
      ix_delete(expression->type.statement.st_for.block);
      break;

    case IXAT_STATEMENT_WHILE:
      ix_delete(expression->type.statement.st_while.condition);
      ix_delete(expression->type.statement.st_while.block);
      break;
    
    case IXAT_STATEMENT_DO_WHILE:
      ix_delete(expression->type.statement.st_do_while.block);
      ix_delete(expression->type.statement.st_do_while.condition);
      break;
    
    case IXAT_STATEMENT_IF:
      ix_delete(expression->type.statement.st_if.condition);
      ix_delete(expression->type.statement.st_if.true_block);
      ix_delete(expression->type.statement.st_if.false_block);
      break;
        
    case IXAT_STATEMENT_LAST:
      ix_delete(expression->type.statement.st_last.expression);
      break;
    
    case IXAT_STATEMENT_FUNCTION:
      ix_delete(expression->type.statement.st_function.function_name);
      ix_delete(expression->type.statement.st_function.parameter_list);
      ix_delete(expression->type.statement.st_function.body);
      break;
    
    case IXAT_STATEMENT_CALL:
      ix_delete(expression->type.statement.st_call.function_name);
      ix_delete(expression->type.statement.st_call.argument_list);
      break;
    
    case IXAT_STATEMENT_DECLARATION:
      ix_delete(expression->type.statement.st_declaration.identifier);
      ix_delete(expression->type.statement.st_declaration.expression);
      break;
    
    case IXAT_VALUE_CONSTANT:
      break;

    case IXAT_VALUE_VARIABLE:
      ix_delete(expression->type.value.variable);
      break;

    case IXAT_VALUE_STRING:
      ix_destroy(&expression->type.value.string);
      break;

    case IXAT_VALUE_INTEGER:
      break;

    case IXAT_VALUE_REAL:
      break;

    default:
      IX_ASSERT(0);
      break;
  }
}

ix_ast_program* ix_ast_program_ctor(ix_ast_program* object)
{
  IX_OBJECT_SET_DTOR(object, &ix_ast_program_dtor);

  return object;
}

void ix_ast_program_dtor(ix_object* object)
{
  ix_ast_program* program = (ix_ast_program*)object;
  
  ix_delete(program->statement_list);
}

ix_ast* ix_ast_ctor(ix_ast* object)
{
  IX_OBJECT_SET_DTOR(object, &ix_ast_dtor);

  return object;
}

void ix_ast_dtor(ix_object* object)
{
  ix_ast* ast = (ix_ast*)object;

  ix_delete(ast->program);
}

void ix_ast_dump(ix_ast* object)
{
  ix_ast_expression_ptr_list_dump(object->program->statement_list, 0);
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
