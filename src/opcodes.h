#pragma once
#ifndef IX_OPCODE_H
#define IX_OPCODE_H

typedef enum ix_tag_op
{
  IXOP_HLT,

  IXOP_PUSHI,   // push integer
  IXOP_PUSHR,   // push real
  IXOP_POP,     // pop element

  IXOP_LD,      // load data to stack
  IXOP_ST,      // store data from stack
  IXOP_STP,     // store data from stack and pop

  IXOP_JMP,
  IXOP_JMPT,    // jump if last condition is true
  IXOP_JMPNT,   // jump if last condition is false
  IXOP_CALL,
  IXOP_RET,

  IXOP_AND,
  IXOP_OR,
  IXOP_XOR,
  IXOP_NOT,

  IXOP_EQ,
  IXOP_NEQ,
  IXOP_LT,
  IXOP_LTE,
  IXOP_GT,
  IXOP_GTE,

  IXOP_ADD,
  IXOP_SUB,
  IXOP_MUL,
  IXOP_DIV,
  IXOP_MOD,

  IXOP_SSET,
  IXOP_SCONCAT,
  IXOP_SSUBSTR,
  IXOP_SSIZE,
} ix_op;

typedef struct ix_tag_op_detail
{
  ix_op opcode;
  const char* name;
  int arg_count;
} ix_op_detail;

// static ix_op_detail ix_opcode_map[] =
// {
//   // opcode          name    arg_count  //
//   //----------------------------------- //
//   { IXOP_HLT,       "hlt"     , 0 },    //
//                                    
//   { IXOP_PUSHI,     "pushi"   , 1 },    // in: i
//   { IXOP_PUSHR,     "pushr"   , 1 },    // in: r
//   { IXOP_POP,       "pop"     , 0 },    // out: i/r/s
//                                    
//   { IXOP_LD,        "ld"      , 1 },    // in: i/r/n
//   { IXOP_ST,        "st"      , 1 },    // expects: i/r/n
//   { IXOP_STP,       "stp"     , 1 },    // out: i/r/n
//                                    
//   { IXOP_JMP,       "jmp"     , 1 },    //
//   { IXOP_JMPT,      "jmpt"    , 1 },    //
//   { IXOP_JMPNT,     "jmpnt"   , 1 },    //
//   { IXOP_CALL,      "call"    , 1 },    // out: i
//   { IXOP_RET,       "ret"     , 0 },    // in: i
//                                    
//   { IXOP_AND,       "and"     , 0 },    // in: i, i; out: i
//   { IXOP_OR,        "or"      , 0 },    // in: i, i; out: i
//   { IXOP_XOR,       "xor"     , 0 },    // in: i, i; out: i
//   { IXOP_NOT,       "not"     , 0 },    // in: i; out: i
//   { IXOP_UNM,       "unm"     , 0 },    // in: i/r; out: i/r
//                                    
//   { IXOP_EQ,        "eq"      , 0 },    // in: i/r/s, i/r/s; out: i
//   { IXOP_NEQ,       "neq"     , 0 },    // in: i/r/s, i/r/s; out: i
//   { IXOP_LT,        "lt"      , 0 },    // in: i/r/s, i/r/s; out: i
//   { IXOP_LTE,       "lte"     , 0 },    // in: i/r/s, i/r/s; out: i
//   { IXOP_GT,        "gt"      , 0 },    // in: i/r/s, i/r/s; out: i
//   { IXOP_GTE,       "gte"     , 0 },    // in: i/r/s, i/r/s; out: i
//                                    
//   { IXOP_ADD,       "add"     , 0 },    // in: i/r, i/r; out: i/r
//   { IXOP_SUB,       "sub"     , 0 },    // in: i/r, i/r; out: i/r
//   { IXOP_MUL,       "mul"     , 0 },    // in: i/r, i/r; out: i/r
//   { IXOP_DIV,       "div"     , 0 },    // in: i/r, i/r; out: i/r
//   { IXOP_MOD,       "mod"     , 0 },    // in: i, i; out: i
//                                    
//   { IXOP_SSET,      "sset"    , 0 },    // in: s(dest), s(src)
//   { IXOP_SCONCAT,   "sconcat" , 0 },    // in: s(dest), s(src1), s(src2)
//   { IXOP_SSUBSTR,   "ssubstr" , 0 },    // in: s(dest), s(src), i(index), i(count)
//   { IXOP_SSIZE,     "ssize"   , 0 },    // in: s; out: i
// };

#endif
