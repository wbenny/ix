#include "mem_stream.h"
#include "list.h"
#include "types.h"

#include <memory.h>

#ifdef IX_COMPILER_MSVC
# pragma region function prototypes
#endif

//////////////////////////////////////////////////////////////////////////
//
// function prototypes
//
//////////////////////////////////////////////////////////////////////////

static size_t    ix_mem_stream_read     (void* object, void* buffer, size_t size);
static size_t    ix_mem_stream_write    (void* object, const void* buffer, size_t size);
static bool      ix_mem_stream_seek     (void* object, ix_stream_origin origin, long offset);
static size_t    ix_mem_stream_resize   (void* object, size_t new_size);
static size_t    ix_mem_stream_size     (void* object);
static size_t    ix_mem_stream_position (void* object);
static void      ix_mem_stream_close    (void* object);

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region private implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// private implementation
//
//////////////////////////////////////////////////////////////////////////

static size_t ix_mem_stream_read(void* object, void* buffer, size_t size)
{
  ix_mem_stream* memstream = (ix_mem_stream*)object;

  size_t bytes_to_end = memstream->buffer.size - memstream->position;
  size_t size_to_read = size < bytes_to_end ? size : bytes_to_end;
  
  memcpy(buffer, memstream->buffer.first + memstream->position, size_to_read);
  memstream->position += size_to_read;

  return size_to_read;
}

static size_t ix_mem_stream_write(void* object, const void* buffer, size_t size)
{
  ix_mem_stream* memstream = (ix_mem_stream*)object;

  ix_list_insert_many(&memstream->buffer, memstream->position, buffer, size);
  memstream->position += size;

  return size;
}

static bool ix_mem_stream_seek(void* object, ix_stream_origin origin, long offset)
{
  ix_mem_stream* memstream = (ix_mem_stream*)object;

  switch (origin)
  {
    case IXSS_SET: memstream->position  = offset; break;
    case IXSS_CUR: memstream->position += offset; break;
    case IXSS_END: memstream->position  = memstream->buffer.size - offset; break;
    default: IX_ASSERT(0); break;
  }

  return true;
}

static size_t ix_mem_stream_resize(void* object, size_t new_size)
{
  ix_mem_stream* memstream = (ix_mem_stream*)object;

  IX_UNUSED(object);
  IX_UNUSED(new_size);

  IX_UNUSED(memstream);

  IX_ASSERT(0 && "not implemented");
  
  return 0;
}

static size_t ix_mem_stream_size(void* object)
{
  ix_mem_stream* memstream = (ix_mem_stream*) object;

  return memstream->buffer.size;
}

static size_t ix_mem_stream_position(void* object)
{
  ix_mem_stream* memstream = (ix_mem_stream*)object;

  return memstream->position;
}

static void ix_mem_stream_close(void* object)
{
  ix_mem_stream* memstream = (ix_mem_stream*)object;

  IX_UNUSED(memstream);
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix_mem_stream* ix_mem_stream_ctor(ix_mem_stream* object)
{
  IX_OBJECT_SET_DTOR(object, &ix_mem_stream_dtor);

  ix_construct_list(uint8_t, &object->buffer, IXLF_NONE);

  object->base.read     = &ix_mem_stream_read;
  object->base.write    = &ix_mem_stream_write;
  object->base.seek     = &ix_mem_stream_seek;
  object->base.resize   = &ix_mem_stream_resize;
  object->base.size     = &ix_mem_stream_size;
  object->base.position = &ix_mem_stream_position;
  object->base.close    = &ix_mem_stream_close;
  object->data          = &ix_mem_stream_data;

  object->position      = 0;

  return object;
}

void ix_mem_stream_dtor(ix_object* object)
{
  ix_mem_stream* memstream = (ix_mem_stream*)object;

  ix_destroy(&memstream->buffer);
}

uint8_t* ix_mem_stream_data(void* object)
{
  ix_mem_stream* memstream = (ix_mem_stream*)object;

  return memstream->buffer.first;
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
