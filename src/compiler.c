#include "compiler.h"

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#ifdef IX_COMPILER_MSVC
# pragma region function prototypes
#endif

//////////////////////////////////////////////////////////////////////////
//
// function prototypes
//
//////////////////////////////////////////////////////////////////////////

static void ix_compiler_compile_symbol_table          (ix_compiler* object, ix_symbol_item_ptr_list* symbol_table);
static void ix_compiler_compile_expression            (ix_compiler* object, ix_ast_expression* expression);
static void ix_compiler_compile_expression_unary      (ix_compiler* object, ix_ast_expression* expression_unary);
static void ix_compiler_compile_expression_binary     (ix_compiler* object, ix_ast_expression* expression_binary);
static void ix_compiler_compile_value_variable        (ix_compiler* object, ix_ast_expression* value_variable);
static void ix_compiler_compile_value_string          (ix_compiler* object, ix_ast_expression* value_variable);
static void ix_compiler_compile_value_integer         (ix_compiler* object, ix_ast_expression* value_number);
static void ix_compiler_compile_value_real            (ix_compiler* object, ix_ast_expression* value_number);
static void ix_compiler_compile_statement_for         (ix_compiler* object, ix_ast_expression* statement_for);
static void ix_compiler_compile_statement_while       (ix_compiler* object, ix_ast_expression* statement_while);
static void ix_compiler_compile_statement_do_while    (ix_compiler* object, ix_ast_expression* statement_do_while);
static void ix_compiler_compile_statement_if          (ix_compiler* object, ix_ast_expression* statement_if);
static void ix_compiler_compile_statement_block       (ix_compiler* object, ix_ast_expression* statement_block);
static void ix_compiler_compile_statement_function    (ix_compiler* object, ix_ast_expression* statement_function);
static void ix_compiler_compile_statement_declaration (ix_compiler* object, ix_ast_expression* statement_declaration);
static void ix_compiler_compile_statement_last        (ix_compiler* object, ix_ast_expression* statement_last);
static void ix_compiler_compile_statement_call        (ix_compiler* object, ix_ast_expression* statement_call);

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region private implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// private implementation
//
//////////////////////////////////////////////////////////////////////////

static void ix_compiler_compile_symbol_table(ix_compiler* object, ix_symbol_item_ptr_list* symbol_table)
{
  ix_symbol_table* symbol_item;
  size_t i;

  ix_variable result;
  ix_construct(ix_variable, &result);

  for (i = 0; i < symbol_table->size; i++)
  {
    symbol_item = ix_list_get(symbol_table, i);

    if (strlen(symbol_item->name))
    {
      ix_variable_set_name(&result, symbol_item->name);

      switch (symbol_item->type)
      {
        case IXST_FUNCTION:
          ix_variable_offset(&result, 0);
          break;

        case IXST_VARIABLE:
          ix_variable_null(&result);
          break;

        case IXST_INTEGER:
          ix_variable_integer(&result, 0);
          break;

        case IXST_REAL:
          ix_variable_real(&result, 0.0);
          break;

        case IXST_STRING:
          ix_variable_char_array(&result, symbol_item->variable.value.value.string.content);
          break;

        default:
          IX_ASSERT(0);
          break;
      }

      ix_emitter_emit_variable(&object->emitter, &result);
    }

    ix_compiler_compile_symbol_table(object, &symbol_item->children);
  }
}

static void ix_compiler_compile_expression(ix_compiler* object, ix_ast_expression* expression)
{
  switch (ix_ast_get_type(expression))
  {
    case IXAT_STATEMENT_BLOCK:
      ix_compiler_compile_statement_block(object, expression);
      break;

    case IXAT_STATEMENT_FOR:
      ix_compiler_compile_statement_for(object, expression);
      break;

    case IXAT_STATEMENT_WHILE:
      ix_compiler_compile_statement_while(object, expression);
      break;

    case IXAT_STATEMENT_DO_WHILE:
      ix_compiler_compile_statement_do_while(object, expression);
      break;

    case IXAT_STATEMENT_IF:
      ix_compiler_compile_statement_if(object, expression);
      break;;

    case IXAT_STATEMENT_LAST:
      ix_compiler_compile_statement_last(object, expression);
      break;

    case IXAT_STATEMENT_FUNCTION:
      ix_compiler_compile_statement_function(object, expression);
      break;

    case IXAT_STATEMENT_CALL:
      ix_compiler_compile_statement_call(object, expression);
      break;

    case IXAT_STATEMENT_DECLARATION:
      ix_compiler_compile_statement_declaration(object, expression);
      break;

    case IXAT_EXPRESSION_UNARY:
      ix_compiler_compile_expression_unary(object, expression);
      break;

    case IXAT_EXPRESSION_BINARY:
      ix_compiler_compile_expression_binary(object, expression);
      break;

    case IXAT_VALUE_VARIABLE:
      ix_compiler_compile_value_variable(object, expression);
      break;

    case IXAT_VALUE_STRING:
      ix_compiler_compile_value_string(object, expression);
      break;

    case IXAT_VALUE_INTEGER:
      ix_compiler_compile_value_integer(object, expression);
      break;

    case IXAT_VALUE_REAL:
      ix_compiler_compile_value_real(object, expression);
      break;

    default:
      IX_ASSERT(0);
      break;
  }
}

static void ix_compiler_compile_expression_unary(ix_compiler* object, ix_ast_expression* expression_unary)
{
  ix_integer_type dummy = 1; // pushi 1

  object->expression_level++;

  ix_emitter_emit(&object->emitter, IXOP_LD,    IXVMAT_LABEL,   expression_unary->type.unary.expression->type.value.variable->identifier_name);
  ix_emitter_emit(&object->emitter, IXOP_LD,    IXVMAT_LABEL,   expression_unary->type.unary.expression->type.value.variable->identifier_name);
  ix_emitter_emit(&object->emitter, IXOP_PUSHI, IXVMAT_INTEGER, &dummy);
  ix_emitter_emit(&object->emitter, IXOP_ADD,   IXVMAT_NONE,    NULL);
  ix_emitter_emit(&object->emitter, IXOP_STP,   IXVMAT_LABEL,   expression_unary->type.unary.expression->type.value.variable->identifier_name);

  object->expression_level--;

  if (!object->expression_level)
  {
    ix_emitter_emit(&object->emitter, IXOP_POP, IXVMAT_NONE, NULL);
  }
}

static void ix_compiler_compile_expression_binary(ix_compiler* object, ix_ast_expression* expression_binary)
{
  object->expression_level++;

  do 
  {
    if (expression_binary->type.binary.operation == IXT_ASSIGN)
    {
      ix_ast_expression* variable = expression_binary->type.binary.left;

      ix_compiler_compile_expression(object, expression_binary->type.binary.right);
      ix_emitter_emit(&object->emitter, IXOP_ST, IXVMAT_LABEL, variable->type.value.variable->identifier_name);
      break;
    }
    else if (expression_binary->type.binary.operation == IXT_PLUS_ASSIGN)
    {
      ix_ast_expression* variable = expression_binary->type.binary.left;

      ix_compiler_compile_expression(object, expression_binary->type.binary.right);
      ix_compiler_compile_expression(object, expression_binary->type.binary.left);
      ix_emitter_emit(&object->emitter, IXOP_ADD, IXVMAT_NONE, NULL);
      ix_emitter_emit(&object->emitter, IXOP_ST, IXVMAT_LABEL, variable->type.value.variable->identifier_name);
      break;
    }

    ix_compiler_compile_expression(object, expression_binary->type.binary.right);
    ix_compiler_compile_expression(object, expression_binary->type.binary.left);

#define IX_COMPILER_MATH_EXP(token, op)                       \
  case token:                                                 \
    ix_emitter_emit(&object->emitter, op, IXVMAT_NONE, NULL); \
    break

    switch (expression_binary->type.binary.operation)
    {
      IX_COMPILER_MATH_EXP(IXT_PLUS,                  IXOP_ADD);
      IX_COMPILER_MATH_EXP(IXT_MINUS,                 IXOP_SUB);
      IX_COMPILER_MATH_EXP(IXT_MULTIPLY,              IXOP_MUL);
      IX_COMPILER_MATH_EXP(IXT_DIVIDE,                IXOP_DIV);
      IX_COMPILER_MATH_EXP(IXT_MODULO,                IXOP_MOD);

      IX_COMPILER_MATH_EXP(IXT_BITWISE_AND,           IXOP_AND);
      IX_COMPILER_MATH_EXP(IXT_BITWISE_OR,            IXOP_OR);
      IX_COMPILER_MATH_EXP(IXT_BITWISE_XOR,           IXOP_XOR);

      IX_COMPILER_MATH_EXP(IXT_EQUAL,                 IXOP_EQ);
      IX_COMPILER_MATH_EXP(IXT_NOT_EQUAL,             IXOP_NEQ);
      IX_COMPILER_MATH_EXP(IXT_LESS_THAN,             IXOP_LT);
      IX_COMPILER_MATH_EXP(IXT_GREATER_THAN,          IXOP_GT);
      IX_COMPILER_MATH_EXP(IXT_LESS_THAN_OR_EQUAL,    IXOP_LTE);
      IX_COMPILER_MATH_EXP(IXT_GREATER_THAN_OR_EQUAL, IXOP_GTE);

      default:

        break;
    }
  } while (0);

  object->expression_level--;

  if (!object->expression_level)
  {
    ix_emitter_emit(&object->emitter, IXOP_POP, IXVMAT_NONE, NULL);
  }
}

static void ix_compiler_compile_value_variable(ix_compiler* object, ix_ast_expression* value_variable)
{
  ix_emitter_emit(&object->emitter, IXOP_LD, IXVMAT_LABEL, value_variable->type.value.variable->identifier_name);
}

static void ix_compiler_compile_value_string(ix_compiler* object, ix_ast_expression* value_variable)
{
  ix_emitter_emit(&object->emitter, IXOP_LD, IXVMAT_LABEL, value_variable->type.value.string.content);
}

static void ix_compiler_compile_value_integer(ix_compiler* object, ix_ast_expression* value_number)
{
  ix_emitter_emit(&object->emitter, IXOP_PUSHI, IXVMAT_INTEGER, &value_number->type.value.integer);
}

static void ix_compiler_compile_value_real(ix_compiler* object, ix_ast_expression* value_number)
{
  ix_emitter_emit(&object->emitter, IXOP_PUSHR, IXVMAT_REAL, &value_number->type.value.real);
}

static void ix_compiler_compile_statement_for(ix_compiler* object, ix_ast_expression* statement_for)
{
  ix_ast_statement_for* for_loop = &statement_for->type.statement.st_for;

  char forBlockName[32];
  char forBlockNameEnd[32];

  sprintf(forBlockName,     "block%" PRIu32, object->label_id++);
  sprintf(forBlockNameEnd,  "block%" PRIu32, object->label_id++);

  // for (###; ...; ...) { ... }
  ix_compiler_compile_expression(object, for_loop->init);
  ix_emitter_emit_label(&object->emitter, forBlockName);

  // for (...; ###; ...) { ... }
  object->expression_level++;
  ix_compiler_compile_expression(object, for_loop->condition);
  object->expression_level--;

  ix_emitter_emit(&object->emitter, IXOP_JMPNT, IXVMAT_JUMP, forBlockNameEnd);

  // for (...; ...; ...) { ### }
  ix_compiler_compile_expression(object, for_loop->block);

  // for (...; ...; ###) { ... }
  ix_compiler_compile_expression(object, for_loop->count);
  ix_emitter_emit(&object->emitter, IXOP_JMP, IXVMAT_JUMP, forBlockName);

  ix_emitter_emit_label(&object->emitter, forBlockNameEnd);
}

static void ix_compiler_compile_statement_while(ix_compiler* object, ix_ast_expression* statement_while)
{
  ix_ast_statement_while* while_loop = &statement_while->type.statement.st_while;

  char whileBlockName[32];
  char whileBlockNameEnd[32];

  sprintf(whileBlockName,    "block_start");
  sprintf(whileBlockNameEnd, "block_end");

  // while (###) { ... }
  ix_emitter_emit_label(&object->emitter, whileBlockName);

  object->expression_level++;
  ix_compiler_compile_expression(object, while_loop->condition);
  object->expression_level--;

  ix_emitter_emit(&object->emitter, IXOP_JMPNT, IXVMAT_JUMP, whileBlockNameEnd);

  // while (...) { ### }
  ix_compiler_compile_expression(object, while_loop->block);
  ix_emitter_emit(&object->emitter, IXOP_JMP, IXVMAT_JUMP, whileBlockName);

  ix_emitter_emit_label(&object->emitter, whileBlockNameEnd);
}

static void ix_compiler_compile_statement_do_while(ix_compiler* object, ix_ast_expression* statement_do_while)
{
  ix_ast_statement_do_while* do_while_loop = &statement_do_while->type.statement.st_do_while;

  char doWhileBlockName[32];

  sprintf(doWhileBlockName, "block%" PRIu32, object->label_id++);

  // do { ### } while (...)
  ix_emitter_emit_label(&object->emitter, doWhileBlockName);

  ix_compiler_compile_expression(object, do_while_loop->block);

  // do { ... } while (###)
  object->expression_level++;
  ix_compiler_compile_expression(object, do_while_loop->condition);
  object->expression_level--;

  ix_emitter_emit(&object->emitter, IXOP_JMPT, IXVMAT_JUMP, doWhileBlockName);
}

static void ix_compiler_compile_statement_if(ix_compiler* object, ix_ast_expression* statement_if)
{
  ix_ast_statement_if* if_condition = &statement_if->type.statement.st_if;

  char ifBlockElse[32];
  char ifBlockNameEnd[32];

  sprintf(ifBlockElse,     "block%" PRIu32, object->label_id++);
  sprintf(ifBlockNameEnd,  "block%" PRIu32, object->label_id++);
  
  //
  // if (###) { ... } else { ... }
  //

  object->expression_level++;
  ix_compiler_compile_expression(object, if_condition->condition);
  object->expression_level--;

  ix_emitter_emit(&object->emitter, IXOP_JMPNT, IXVMAT_JUMP, ifBlockElse);

  //
  // if (...) { ### } else { ... }
  //
  
  ix_compiler_compile_expression(object, if_condition->true_block);

  //
  // after executing true_block jump at the end,
  // otherwise false_block would be executed too
  //
  
  ix_emitter_emit(&object->emitter, IXOP_JMP, IXVMAT_JUMP, ifBlockNameEnd);

  //
  // mark where false_block starts
  //

  ix_emitter_emit_label(&object->emitter, ifBlockElse);

  if (if_condition->false_block)
  {
    //
    // if (...) { ... } else { ### }
    //
    
    ix_compiler_compile_expression(object, if_condition->false_block);
  }

  ix_emitter_emit_label(&object->emitter, ifBlockNameEnd);
}

static void ix_compiler_compile_statement_block(ix_compiler* object, ix_ast_expression* statement_block)
{
  size_t i;

  for (i = 0; i < statement_block->type.statement.st_block.statement_list->size; i++)
  {
    ix_compiler_compile_expression(object, ix_list_get(statement_block->type.statement.st_block.statement_list, i));
  }
}

static void ix_compiler_compile_statement_function(ix_compiler* object, ix_ast_expression* statement_function)
{
  ix_ast_statement_function* function = &statement_function->type.statement.st_function;
  size_t param_count;

  ix_emitter_emit_label(&object->emitter, function->function_name->identifier_name);

  ix_emitter_emit(&object->emitter, IXOP_POP, IXVMAT_NONE, NULL); // argument count

  param_count = function->parameter_list->size;
  while (param_count--)
  {
    ix_ast_identifier* parameter_name = ix_list_get(function->parameter_list, param_count);
    ix_emitter_emit(&object->emitter, IXOP_STP, IXVMAT_LABEL, parameter_name->identifier_name);
  }

  ix_compiler_compile_statement_block(object, function->body);
}

static void ix_compiler_compile_statement_declaration(ix_compiler* object, ix_ast_expression* statement_declaration)
{
  if (statement_declaration->type.statement.st_declaration.expression)
  {
    ix_compiler_compile_expression(object, statement_declaration->type.statement.st_declaration.expression);
  }
}

static void ix_compiler_compile_statement_last(ix_compiler* object, ix_ast_expression* statement_last)
{
  switch (statement_last->type.statement.st_last.keyword)
  {
    case IXT_KW_CONTINUE:
      ix_emitter_emit(&object->emitter, IXOP_JMP, IXVMAT_JUMP, "block_start");
      break;

    case IXT_KW_BREAK:
      ix_emitter_emit(&object->emitter, IXOP_JMP, IXVMAT_JUMP, "block_end");
      break;
  
    case IXT_KW_RETURN:
      if (statement_last->type.statement.st_last.expression)
      {
        object->expression_level++;
        ix_compiler_compile_expression(object, statement_last->type.statement.st_last.expression);
        object->expression_level--;
      }

      ix_emitter_emit(&object->emitter, IXOP_RET, IXVMAT_NONE, NULL);
      break;

    default:
      IX_ASSERT(0);
      break;
  }
}

static void ix_compiler_compile_statement_call(ix_compiler* object, ix_ast_expression* statement_call)
{
  ix_ast_statement_call* call = &statement_call->type.statement.st_call;
  size_t i;
  ix_integer_type arg_list_size = call->argument_list->size;

  for (i = 0; i < (size_t)arg_list_size; i++)
  {
    object->expression_level++;
    ix_compiler_compile_expression(object, ix_list_get(call->argument_list, i));
    object->expression_level--;
  }

  ix_emitter_emit(&object->emitter, IXOP_PUSHI, IXVMAT_INTEGER, &arg_list_size);
  ix_emitter_emit(&object->emitter, IXOP_CALL,  IXVMAT_LABEL,   call->function_name->identifier_name);
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix_compiler* ix_compiler_ctor(ix_compiler* object, ix_error_list* errors, ix_stream* code_stream)
{
  IX_OBJECT_SET_DTOR(object, &ix_compiler_dtor);

  ix_construct(ix_emitter, &object->emitter, code_stream);

  object->label_id = 0;
  object->expression_level = 0;

  object->errors = errors;

  return object;
}

void ix_compiler_dtor(ix_object* object)
{
  ix_compiler* compiler = (ix_compiler*)object;

  ix_destroy(&compiler->emitter);
}

void ix_compiler_compile(ix_compiler* object, ix_ast* ast, ix_symbol_table* symbol_table)
{
  size_t i;

  ix_compiler_compile_symbol_table(object, &symbol_table->children);
  ix_emitter_mark_end_of_data_section(&object->emitter);

  for (i = 0; i < ast->program->statement_list->size; i++)
  {
    ix_compiler_compile_expression(object, ix_list_get(ast->program->statement_list, i));
  }

  ix_emitter_fix_jumps(&object->emitter);
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
