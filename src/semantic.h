#pragma once
#ifndef IX_SEMANTIC_H
#define IX_SEMANTIC_H

#include "ast.h"
#include "variable.h"
#include "symbol.h"
#include "error.h"
#include "types.h"

typedef struct ix_tag_semantic
{
  IX_OBJECT;

  ix_error_list*    errors;

  ix_symbol_table*  symbol_table;
} ix_semantic;

ix_semantic*  ix_semantic_ctor    (ix_semantic* object, ix_error_list* errors);
void          ix_semantic_dtor    (ix_object* object);

void          ix_semantic_analyze (ix_semantic* object, ix_ast* ast, ix_symbol_table* symbol_table);

#endif
