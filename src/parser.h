#pragma once
#ifndef IX_PARSER_H
#define IX_PARSER_H

#include "ast.h"
#include "token.h"
#include "error.h"
#include "types.h"

typedef struct ix_tag_parser
{
  IX_OBJECT;

  ix_error_list*    errors;

  struct
  {
    ix_token_list*  token_list;
    int             position;
  } parser_context;
} ix_parser;

ix_parser*  ix_parser_ctor            (ix_parser* object, ix_error_list* errors);
void        ix_parser_dtor            (ix_object* object);

void        ix_parser_parse_token_list(ix_parser* object, ix_token_list* token_list, ix_ast* ast);

#endif
