#pragma once
#ifndef IX_AST_H
#define IX_AST_H

#include "list.h"
#include "variable.h"
#include "token.h"

#define ix_ast_set_type(ast_item, new_type) (((ix_ast_item*)ast_item)->type = (new_type))
#define ix_ast_get_type(ast_item)           (((ix_ast_item*)ast_item)->type)

typedef enum ix_tag_ast_type
{
  IXAT_NONE,
  IXAT_IDENTIFIER,
  
  IXAT_EXPRESSION_UNARY,
  IXAT_EXPRESSION_BINARY,
  
  IXAT_STATEMENT_BLOCK,
  IXAT_STATEMENT_FOR,
  IXAT_STATEMENT_WHILE,
  IXAT_STATEMENT_DO_WHILE,
  IXAT_STATEMENT_IF,
  IXAT_STATEMENT_LAST,
  IXAT_STATEMENT_FUNCTION,
  IXAT_STATEMENT_CALL,
  IXAT_STATEMENT_DECLARATION,

  IXAT_VALUE_CONSTANT,
  IXAT_VALUE_VARIABLE,
  IXAT_VALUE_STRING,
  IXAT_VALUE_INTEGER,
  IXAT_VALUE_REAL,

  IXAT_PROGRAM,
} ix_ast_type;

typedef struct ix_tag_ast_item                    ix_ast_item;
typedef struct ix_tag_ast_identifier              ix_ast_identifier;
typedef union  ix_tag_ast_value                   ix_ast_value;

typedef struct ix_tag_ast_expression              ix_ast_expression;
typedef struct ix_tag_ast_expression_unary        ix_ast_expression_unary;
typedef struct ix_tag_ast_expression_binary       ix_ast_expression_binary;

typedef struct ix_tag_ast_statement_block         ix_ast_statement_block;
typedef struct ix_tag_ast_statement_for           ix_ast_statement_for;
typedef struct ix_tag_ast_statement_while         ix_ast_statement_while;
typedef struct ix_tag_ast_statement_do_while      ix_ast_statement_do_while;
typedef struct ix_tag_ast_statement_if            ix_ast_statement_if;
typedef struct ix_tag_ast_statement_last          ix_ast_statement_last;
typedef struct ix_tag_ast_statement_function      ix_ast_statement_function;
typedef struct ix_tag_ast_statement_call          ix_ast_statement_call;
typedef struct ix_tag_ast_statement_declaration   ix_ast_statement_declaration;

typedef struct ix_tag_ast_program                 ix_ast_program;

typedef struct ix_tag_ast                         ix_ast;

typedef ix_list ix_ast_identifier_list;
typedef ix_list ix_ast_expression_ptr_list;

struct ix_tag_ast_item
{
  IX_OBJECT;

  ix_ast_type type;
};

struct ix_tag_ast_identifier
{
  ix_ast_item base;

  char identifier_name[32];
};

union ix_tag_ast_value
{
  ix_token_type       constant;
  ix_ast_identifier*  variable;
  ix_string_type      string;
  ix_integer_type     integer;
  ix_real_type        real;
};

struct ix_tag_ast_expression_unary
{
  ix_ast_expression*  expression;
  ix_token_type       operation;
};

struct ix_tag_ast_expression_binary
{
  ix_ast_expression*  left;
  ix_ast_expression*  right;
  ix_token_type       operation;
};

struct ix_tag_ast_statement_block
{
  ix_ast_expression_ptr_list* statement_list;
};

struct ix_tag_ast_statement_for
{
  ix_ast_expression*  init;
  ix_ast_expression*  condition;
  ix_ast_expression*  count;
  ix_ast_expression*  block;
};

struct ix_tag_ast_statement_while
{
  ix_ast_expression*  condition;
  ix_ast_expression*  block;
};

struct ix_tag_ast_statement_do_while
{
  ix_ast_expression*  block;
  ix_ast_expression*  condition;
};

struct ix_tag_ast_statement_if
{
  ix_ast_expression*  condition;
  ix_ast_expression*  true_block;
  ix_ast_expression*  false_block;
};

struct ix_tag_ast_statement_last
{
  ix_token_type       keyword;
  ix_ast_expression*  expression;
};

struct ix_tag_ast_statement_function
{
  ix_ast_identifier*  function_name;
  ix_ast_identifier_list* parameter_list;
  ix_ast_expression*  body;
};

struct ix_tag_ast_statement_call
{
  ix_ast_identifier*  function_name;
  ix_ast_expression_ptr_list* argument_list;
};

struct ix_tag_ast_statement_declaration
{
  ix_ast_identifier*  identifier;
  ix_ast_expression*  expression;
};

struct ix_tag_ast_expression
{
  ix_ast_item base;

  union
  {
    ix_ast_value                    value;

    ix_ast_expression_unary         unary;
    ix_ast_expression_binary        binary;

    union
    {
      ix_ast_statement_block        st_block;
      ix_ast_statement_for          st_for;
      ix_ast_statement_while        st_while;
      ix_ast_statement_do_while     st_do_while;
      ix_ast_statement_if           st_if;
      ix_ast_statement_last         st_last;
      ix_ast_statement_function     st_function;
      ix_ast_statement_call         st_call;
      ix_ast_statement_declaration  st_declaration;
    } statement;
  } type;
};

struct ix_tag_ast_program
{
  ix_ast_item base;

  ix_ast_expression_ptr_list* statement_list;
};

struct ix_tag_ast
{
  IX_OBJECT;

  ix_ast_program*  program;
};

ix_ast_identifier*  ix_ast_identifier_ctor(ix_ast_identifier* object);
void                ix_ast_identifier_dtor(ix_object* object);

ix_ast_expression*  ix_ast_expression_ctor(ix_ast_expression* object);
void                ix_ast_expression_dtor(ix_object* object);

ix_ast_program*     ix_ast_program_ctor   (ix_ast_program* object);
void                ix_ast_program_dtor   (ix_object* object);

ix_ast*             ix_ast_ctor           (ix_ast* object);
void                ix_ast_dtor           (ix_object* object);

//
// dump the whole ast
//

void ix_ast_dump(ix_ast* object);

#endif
