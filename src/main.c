#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "ix.h"
#include "file_stream.h"

#ifdef IX_COMPILER_MSVC
# define IX_CDECL __cdecl
# define IX_DUMP_MEMLEAKS() _CrtDumpMemoryLeaks()
#else
# define IX_CDECL
# define IX_DUMP_MEMLEAKS()
#endif

int extern_print(ix_value_list* args)
{
  ix_value* val = ix_list_get(args, 0);

  switch (val->type)
  {
    case IXVT_INTEGER: printf("%" PRId64 "\n",    val->value.integer); break;
    case IXVT_REAL:    printf("%f\n",             val->value.real); break;
    case IXVT_STRING:  printf("'%s'\n",           val->value.string.content); break;
    default: break;
  }

  return 0;
}

int IX_CDECL main()
{
  ix ix_main;
  ix_file_stream stream;
  ix_error* err;

  ix_construct(ix, &ix_main);

  ix_construct(ix_file_stream, &stream);
  ix_file_stream_open(&stream, "../test/test.txt", "r");
  ix_process_stream(&ix_main, &stream.base);
  ix_destroy(&stream);

  ix_list_foreach(err, &ix_main.errors)
  {
    printf("error at [%i;%i], unexpected token: '%s'\n", err->source_info.line, err->source_info.column, err->type.parser.unexpected_token->token_value);
  }
 
  ix_register_function(&ix_main, "print", &extern_print);

  if (ix_main.errors.size == 0)
    ix_run(&ix_main);

  ix_destroy(&ix_main);

  // IX_DUMP_MEMLEAKS();

  return 0;
}
