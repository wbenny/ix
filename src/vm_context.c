#include "vm_context.h"

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix_vm_context* ix_vm_context_ctor(ix_vm_context* object)
{
  IX_OBJECT_SET_DTOR(object, &ix_vm_context_dtor);

  ix_construct(ix_mem_stream,       &object->code);
  ix_construct_list(ix_variable,    &object->data,      IXLF_OBJECT);
  ix_construct_list(ix_value,       &object->stack,     IXLF_OBJECT);
  ix_construct_list(ix_offset_type, &object->callstack, IXLF_NONE);

  ix_vm_context_reset(object);

  return object;
}

void ix_vm_context_dtor(ix_object* object)
{
  ix_vm_context* vm_context = (ix_vm_context*)object;

  ix_destroy(&vm_context->callstack);
  ix_destroy(&vm_context->stack);
  ix_destroy(&vm_context->data);
  ix_destroy(&vm_context->code);
}

void ix_vm_context_stack_push(ix_vm_context* object, ix_value* value)
{
  ix_list_add(&object->stack, value);
}

void ix_vm_context_stack_pop(ix_vm_context* object, ix_value* result)
{
  ix_list_pop_copy(&object->stack, result);
}

ix_value* ix_vm_context_stack_top(ix_vm_context* object)
{
  return ix_list_top(&object->stack);
}

void ix_vm_context_call(ix_vm_context* object, ix_offset_type offset)
{
  ix_offset_type return_address = object->ip + sizeof(ix_offset_type);
  ix_list_add(&object->callstack, &return_address);
  object->ip = offset;
}

bool ix_vm_context_return(ix_vm_context* object)
{
  if (ix_list_is_empty(&object->callstack)) return false;

  ix_list_pop_copy(&object->callstack, &object->ip);

  return true;
}

void ix_vm_context_jmp(ix_vm_context* object, ix_offset_type offset)
{
  object->ip = offset;
}

void* ix_vm_context_peek_next_impl(ix_vm_context* object)
{
  // next offset is after current instruction
  return &(ix_mem_stream_data(&object->code)[object->ip + 1]);
}

void* ix_vm_context_read_next_impl(ix_vm_context* object, size_t offset)
{
  void* result = ix_vm_context_peek_next_impl(object);
  object->ip += (ix_offset_type)offset;
  return result;
}

ix_op ix_vm_context_peek_instruction(ix_vm_context* object)
{
  return ix_mem_stream_data(&object->code)[object->ip];
}

void ix_vm_context_reset(ix_vm_context* object)
{
  object->ip = 0;
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
