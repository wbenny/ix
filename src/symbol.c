#include "symbol.h"

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix_symbol_table* ix_symbol_table_ctor(ix_symbol_table* object, ix_symbol_type type)
{
  IX_OBJECT_SET_DTOR(object, &ix_symbol_table_dtor);

  ix_construct(ix_variable, &object->variable);
  ix_construct_list(ix_symbol_table*, &object->children, IXLF_OBJECT | IXLF_POINTER);
  
  object->type = type;
  
  return object;
}

void ix_symbol_table_dtor(ix_object* object)
{
  ix_symbol_table* symbol_table = (ix_symbol_table*)object;

  ix_destroy(&symbol_table->children);
  ix_destroy(&symbol_table->variable);
}

void ix_symbol_table_set_parent(ix_symbol_table* object, ix_symbol_table* new_parent)
{
  object->parent = new_parent;

  ix_list_add(&object->parent->children, object);
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
