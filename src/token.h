#pragma once
#ifndef IX_TOKEN_H
#define IX_TOKEN_H

#include "list.h"
#include "types.h"

#include <stdint.h>

#ifdef IX_COMPILER_MSVC
# pragma region token types
#endif

//////////////////////////////////////////////////////////////////////////
//
// token types
//
//////////////////////////////////////////////////////////////////////////

typedef enum ix_tag_token_type
{
  IXT_NONE,
  IXT_INVALID,
  IXT_BEGIN,
  IXT_END,

  IXT_IDENTIFIER,

  IXT_CHARACTER,
  IXT_STRING,
  IXT_INTEGER,
  IXT_REAL,

  IXT_KW_FUNCTION,            // function
  IXT_KW_VAR,                 // var
  IXT_KW_NULL,                // null
  IXT_KW_TRUE,                // true
  IXT_KW_FALSE,               // false
  IXT_KW_IF,                  // if
  IXT_KW_ELSE,                // else
  IXT_KW_FOR,                 // for
  IXT_KW_DO,                  // do
  IXT_KW_WHILE,               // while
  IXT_KW_SWITCH,              // switch
  IXT_KW_CASE,                // case
  IXT_KW_CONTINUE,            // continue
  IXT_KW_BREAK,               // break
  IXT_KW_RETURN,              // return
  
  IXT_PLUS,                   // +
  IXT_MINUS,                  // -
  IXT_MULTIPLY,               // *
  IXT_DIVIDE,                 // /
  IXT_MODULO,                 // %

  IXT_ASSIGN,                 // =
  IXT_PLUS_ASSIGN,            // +=
  IXT_MINUS_ASSIGN,           // -=
  IXT_MULTIPLY_ASSIGN,        // *=
  IXT_DIVIDE_ASSIGN,          // /=
  IXT_MODULO_ASSIGN,          // %=

  IXT_INCREMENT,              // ++
  IXT_DECREMENT,              // --

  IXT_NOT,                    // !

  IXT_EQUAL,                  // ==
  IXT_NOT_EQUAL,              // !=
  IXT_LESS_THAN,              // <
  IXT_GREATER_THAN,           // >
  IXT_LESS_THAN_OR_EQUAL,     // <=
  IXT_GREATER_THAN_OR_EQUAL,  // >=

  IXT_BITWISE_NOT,            // ~
  IXT_BITWISE_AND,            // &
  IXT_BITWISE_OR,             // |
  IXT_BITWISE_XOR,            // ^

  IXT_BITWISE_AND_ASSIGN,     // &=
  IXT_BITWISE_OR_ASSIGN,      // |=
  IXT_BITWISE_XOR_ASSIGN,     // ^=

  IXT_LOGICAL_AND,            // &&
  IXT_LOGICAL_OR,             // ||

  IXT_BIT_SHIFT_LEFT,         // <<
  IXT_BIT_SHIFT_RIGHT,        // >>

  IXT_BIT_SHIFT_LEFT_ASSIGN,  // <<=
  IXT_BIT_SHIFT_RIGHT_ASSIGN, // >>=

  IXT_LEFT_PARENTHESIS,       // (
  IXT_RIGHT_PARENTHESIS,      // )

  IXT_LEFT_BRACKET,           // [
  IXT_RIGHT_BRACKET,          // ]

  IXT_LEFT_BRACE,             // {
  IXT_RIGHT_BRACE,            // }

  IXT_COMMA,                  // ,
  IXT_SEMICOLON,              // ;
} ix_token_type;

typedef struct ix_tag_source_info
{ 
  uint32_t line;
  uint32_t column;
} ix_source_info;

typedef struct ix_tag_token
{
  ix_token_type   token_type;
  char            token_value[32];
  ix_source_info  source_info;
} ix_token;

typedef ix_list ix_token_list;

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region states
#endif

//////////////////////////////////////////////////////////////////////////
//
// states
//
//////////////////////////////////////////////////////////////////////////

typedef struct ix_tag_token_state_item
{
  ix_token_type current_state;
  char          character;
  ix_token_type token_type;
  ix_token_type next_state;
} ix_token_state_item;

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region keywords
#endif

//////////////////////////////////////////////////////////////////////////
//
// keywords
//
//////////////////////////////////////////////////////////////////////////

typedef struct ix_tag_token_keyword_item
{
  ix_token_type token_type;
  const char*   keyword_literal;
} ix_token_keyword_item;

extern ix_token_state_item ix_token_state_table[];
extern const size_t ix_token_state_table_size;

extern ix_token_keyword_item ix_token_keyword_table[];
extern const size_t ix_token_keyword_table_size;

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#endif
