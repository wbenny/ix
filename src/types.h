#ifndef IX_TYPES_H
#define IX_TYPES_H
#pragma once

#include <memory.h>
#include <stdlib.h>
#include <assert.h>

#ifdef _MSC_VER
# define IX_COMPILER_MSVC

# define _CRTDBG_MAP_ALLOC
# include <crtdbg.h>
#endif

//////////////////////////////////////////////////////////////////////////

typedef enum { false, true } bool;

typedef struct ix_tag_object ix_object;

struct ix_tag_object
{
  void (*dtor)(ix_object* object);
};

#define IX_OBJECT                                     ix_object base
#define IX_OBJECT_SET_DTOR(object, func)              ((((ix_object*)(object))->dtor) = (func))

//////////////////////////////////////////////////////////////////////////

#define IX_MACRO_EXPAND(expr)                         expr
#define IX_MACRO_DISPATCHER(func, ...)                IX_MACRO_EXPAND(IX_MACRO_DISPATCHER2(func, IX_VA_NUM_ARGS(func ## _NUM_ARGS_TABLE, __VA_ARGS__)))
#define IX_MACRO_DISPATCHER2(func, nargs)             IX_MACRO_DISPATCHER3(func, nargs)
#define IX_MACRO_DISPATCHER3(func, nargs)             func ## nargs

#define IX_VA_NUM_ARGS_IMPL(_1, _2, _3, _4, N, ...)   N
#define IX_VA_NUM_ARGS(table, ...)                    IX_MACRO_EXPAND(IX_VA_NUM_ARGS_IMPL(__VA_ARGS__, table))

#define IX_VA_FUNCTION(function, ...)                 IX_MACRO_EXPAND(IX_MACRO_DISPATCHER(function, __VA_ARGS__)(__VA_ARGS__))

#define IX_NEW_NUM_ARGS_TABLE                         1, 1, 1, 0, 0
#define IX_CONSTRUCT_NUM_ARGS_TABLE                   1, 1, 0, 0, 0

#define IX_CONSTRUCT_HELPER0(class, var)              class##_ctor((var))
#define IX_CONSTRUCT_HELPER1(class, var, ...)         class##_ctor((var), __VA_ARGS__)

#define IX_CONSTRUCT_MACRO_HELPER0(class, var)        (memset(var, 0, sizeof(class)), IX_CONSTRUCT_HELPER0(class, var))
#define IX_CONSTRUCT_MACRO_HELPER1(class, var, ...)   (memset(var, 0, sizeof(class)), IX_CONSTRUCT_HELPER1(class, var, __VA_ARGS__))

#define IX_CONSTRUCT0(class, var)                     IX_CONSTRUCT_MACRO_HELPER0(class, var)
#define IX_CONSTRUCT1(class, var, ...)                IX_CONSTRUCT_MACRO_HELPER1(class, var, __VA_ARGS__)

#define IX_NEW0(class)                                IX_CONSTRUCT_HELPER0(class, ix_alloc(class))
#define IX_NEW1(class, ...)                           IX_CONSTRUCT_HELPER1(class, ix_alloc(class), __VA_ARGS__)

//////////////////////////////////////////////////////////////////////////

#define IX_IN
#define IX_OUT

#define IX_UNUSED(x)                          ((void)(x))
#define IX_ASSERT(expr)                       assert(expr)

#define ix_count_of(x)                        ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))

// use with any type
#define ix_alloc_byte_array(size)             (memset(malloc(size), 0, (size)))
#define ix_alloc_array(type, size)            ix_alloc_byte_array(sizeof(type) * (size))
#define ix_alloc(type)                        ix_alloc_byte_array(sizeof(type))
#define ix_free(ptr)                          (free(ptr)/*, ptr = NULL*/)

// use with ix_object
#define ix_construct(...)                     IX_VA_FUNCTION(IX_CONSTRUCT, __VA_ARGS__)
#define ix_destroy(var)                       do { ix_object* var__ = (ix_object*)(var); if (var__->dtor) var__->dtor(var__); } while (0)

#define ix_new(...)                           IX_VA_FUNCTION(IX_NEW, __VA_ARGS__)
#define ix_delete(var)                        do { if (var) { ix_destroy(var); ix_free(var); } } while (0)

#endif
