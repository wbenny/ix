#pragma once
#ifndef IX_STREAM_H
#define IX_STREAM_H

#include "types.h"

typedef enum ix_tag_stream_origin
{
  IXSS_SET,
  IXSS_CUR,
  IXSS_END,
} ix_stream_origin;

typedef struct ix_tag_stream ix_stream;

struct ix_tag_stream
{
  IX_OBJECT;

  size_t (*read)    (void* object, void* buffer, size_t size);
  size_t (*write)   (void* object, const void* buffer, size_t size);
  bool   (*seek)    (void* object, ix_stream_origin origin, long offset);
  size_t (*resize)  (void* object, size_t new_size);
  size_t (*size)    (void* object);
  size_t (*position)(void* object);
  void   (*close)   (void* object);
};

size_t  ix_stream_read      (void* object, void* buffer, size_t size);
size_t  ix_stream_write     (void* object, const void* buffer, size_t size);
bool    ix_stream_seek      (void* object, ix_stream_origin origin, long offset);
size_t  ix_stream_resize    (void* object, size_t new_size);
size_t  ix_stream_size      (void* object);
size_t  ix_stream_position  (void* object);
void    ix_stream_close     (void* object);

size_t  ix_stream_copy_from (void* object, void* source_stream);

#endif
