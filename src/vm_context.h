#pragma once
#ifndef IX_VM_CONTEXT_H
#define IX_VM_CONTEXT_H

#include "list.h"
#include "variable.h"
#include "opcodes.h"
#include "mem_stream.h"
#include "types.h"

#include <stdint.h>

typedef ix_list ix_callstack;

typedef struct ix_tag_vm_context
{
  IX_OBJECT;

  ix_mem_stream     code;
  ix_variable_list  data;
  ix_value_list     stack;
  ix_callstack      callstack;
  ix_offset_type    ip;
} ix_vm_context;

ix_vm_context*  ix_vm_context_ctor            (ix_vm_context* object);
void            ix_vm_context_dtor            (ix_object* object);

void            ix_vm_context_stack_push      (ix_vm_context* object, ix_value* value);
void            ix_vm_context_stack_pop       (ix_vm_context* object, ix_value* result);
ix_value*       ix_vm_context_stack_top       (ix_vm_context* object);
void            ix_vm_context_call            (ix_vm_context* object, ix_offset_type offset);
bool            ix_vm_context_return          (ix_vm_context* object);
void            ix_vm_context_jmp             (ix_vm_context* object, ix_offset_type offset);
void*           ix_vm_context_peek_next_impl  (ix_vm_context* object);
void*           ix_vm_context_read_next_impl  (ix_vm_context* object, size_t offset);
ix_op           ix_vm_context_peek_instruction(ix_vm_context* object);
void            ix_vm_context_reset           (ix_vm_context* object);

#define ix_vm_context_peek_next(vmctx, type) (*(type*)(ix_vm_context_peek_next_impl(vmctx)))
#define ix_vm_context_read_next(vmctx, type) (*(type*)(ix_vm_context_read_next_impl((vmctx), sizeof(type))))

#endif
