#pragma once
#ifndef IX_SYMBOL_H
#define IX_SYMBOL_H

#include "variable.h"
#include "list.h"

typedef enum ix_tag_symbol_type
{
  IXST_NONE,
  IXST_FUNCTION,
  IXST_BLOCK,
  IXST_PROGRAM,
  IXST_VARIABLE,
  IXST_INTEGER,
  IXST_REAL,
  IXST_STRING,
} ix_symbol_type;

struct ix_tag_symbol_item;
typedef struct ix_tag_symbol_item ix_symbol_table;

typedef ix_list ix_symbol_item_ptr_list;

struct ix_tag_symbol_item
{
  IX_OBJECT;

  ix_symbol_type          type;
  char                    name[32];
  ix_variable             variable;

  ix_symbol_table*        parent;
  ix_symbol_item_ptr_list children;
};

ix_symbol_table*  ix_symbol_table_ctor      (ix_symbol_table* object, ix_symbol_type type);
void              ix_symbol_table_dtor      (ix_object* object);

void              ix_symbol_table_set_parent(ix_symbol_table* object, ix_symbol_table* new_parent);

#endif
