#include "ix.h"

#include "lexer.h"
#include "parser.h"
#include "semantic.h"
#include "compiler.h"
#include "ast.h"
#include "symbol.h"
#include "mem_stream.h"

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix* ix_ctor(ix* object)
{
  IX_OBJECT_SET_DTOR(object, &ix_dtor);

  ix_construct_list(ix_error, &object->errors, IXLF_NONE);
  ix_construct(ix_vm, &object->vm, &object->errors);

  return object;
}

void ix_dtor(ix_object* object)
{
  ix* ix_main = (ix*)object;

  ix_destroy(&ix_main->vm);
  ix_destroy(&ix_main->errors);
}

void ix_process_stream(ix* object, ix_stream* stream)
{
  ix_lexer        lexer;
  ix_parser       parser;
  ix_semantic     semantic;
  ix_compiler     compiler;

  ix_ast          ast;
  ix_symbol_table symbol_table;

  ix_token_list   token_list;

  ix_mem_stream   code_stream;

  ix_construct(ix_mem_stream,   &code_stream);
  ix_construct(ix_lexer,        &lexer,         &object->errors);
  ix_construct(ix_parser,       &parser,        &object->errors);
  ix_construct(ix_semantic,     &semantic,      &object->errors);
  ix_construct(ix_compiler,     &compiler,      &object->errors, &code_stream.base);
  ix_construct(ix_symbol_table, &symbol_table,  IXST_PROGRAM);
  ix_construct(ix_ast,          &ast);
  ix_construct_list(ix_token,   &token_list,    IXLF_NONE);

  ix_try
  {
    ix_lexer_process_stream   (&lexer,      stream,       &token_list);
    ix_parser_parse_token_list(&parser,     &token_list,  &ast);
    ix_semantic_analyze       (&semantic,   &ast,         &symbol_table);
    ix_compiler_compile       (&compiler,   &ast,         &symbol_table);

    ix_stream_seek            (&code_stream, IXSS_SET, 0);
    ix_vm_parse_bytecode      (&object->vm, (ix_stream*)&code_stream);

#ifdef _DEBUG
    ix_ast_dump(&ast);
#endif
    
  }
  ix_catch
  {

  }

  ix_destroy(&token_list);
  ix_destroy(&ast);
  ix_destroy(&symbol_table);
  ix_destroy(&compiler);
  ix_destroy(&semantic);
  ix_destroy(&parser);
  ix_destroy(&lexer);
  ix_destroy(&code_stream);
}

void ix_register_function(ix* object, const char* function_name, ix_extern_callback* callback)
{
  ix_vm_register_function(&object->vm, function_name, callback);
}

void ix_run(ix* object)
{
  ix_vm_run(&object->vm);
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
