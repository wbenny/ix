#pragma once
#ifndef IX_VARIABLE_H
#define IX_VARIABLE_H

#include "list.h"
#include "string.h"
#include "types.h"

#include <stdint.h>

typedef int64_t   ix_integer_type;
typedef double    ix_real_type;
typedef ix_string ix_string_type;
typedef int32_t   ix_offset_type;

typedef enum ix_tag_variable_type
{
  IXVT_NULL       = 0x00,
  IXVT_FUNCTION   = 0x01,
  IXVT_INTEGER    = 0x02,
  IXVT_REAL       = 0x04,
  IXVT_STRING     = 0x08,
} ix_value_type;

typedef struct ix_tag_value
{
  IX_OBJECT;

  ix_value_type     type;

  union
  {
    ix_offset_type  offset;
    ix_integer_type integer;
    ix_real_type    real;
    ix_string_type  string;
  } value;
} ix_value;

typedef struct ix_tag_variable
{
  IX_OBJECT;

  char              name[32];
  uint32_t          name_hash;
  ix_value          value;
} ix_variable;

typedef ix_list ix_value_list;
typedef ix_list ix_variable_list;

//////////////////////////////////////////////////////////////////////////

ix_value*     ix_value_ctor         (ix_value* object);
void          ix_value_dtor         (ix_object* object);
void          ix_value_copy_from    (ix_value* object, ix_value* source);

void          ix_value_null         (ix_value* object);
void          ix_value_offset       (ix_value* object, ix_offset_type offset);
void          ix_value_integer      (ix_value* object, ix_integer_type integer);
void          ix_value_real         (ix_value* object, ix_real_type real);
void          ix_value_string       (ix_value* object, ix_string* string);
void          ix_value_char_array   (ix_value* object, const char* string);

//////////////////////////////////////////////////////////////////////////

ix_variable*  ix_variable_ctor      (ix_variable* object);
void          ix_variable_dtor      (ix_object* object);
void          ix_variable_copy_from (ix_variable* object, ix_variable* source);

void          ix_variable_set_name  (ix_variable* object, const char* name);
void          ix_variable_set_value (ix_variable* object, ix_value* value);

void          ix_variable_null      (ix_variable* object);
void          ix_variable_offset    (ix_variable* object, ix_offset_type offset);
void          ix_variable_integer   (ix_variable* object, ix_integer_type integer);
void          ix_variable_real      (ix_variable* object, ix_real_type real);
void          ix_variable_string    (ix_variable* object, ix_string* string);
void          ix_variable_char_array(ix_variable* object, const char* string);

#endif
