#pragma once
#ifndef IX_LEXER_H
#define IX_LEXER_H

#include "list.h"
#include "token.h"
#include "stream.h"
#include "error.h"
#include "types.h"

typedef struct ix_tag_lexer
{
  IX_OBJECT;

  ix_error_list*  errors;

  ix_stream*      stream;
  ix_source_info  source_info;
} ix_lexer;

ix_lexer* ix_lexer_ctor           (ix_lexer* object, ix_error_list* errors);
void      ix_lexer_dtor           (ix_object* object);

void      ix_lexer_process_stream (ix_lexer* object, ix_stream* stream, ix_token_list* token_list);

#endif
