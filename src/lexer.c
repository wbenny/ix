#include "lexer.h"

#include <string.h>
#include <ctype.h>

#ifdef IX_COMPILER_MSVC
# pragma region function prototypes
#endif

//////////////////////////////////////////////////////////////////////////
//
// function prototypes
//
//////////////////////////////////////////////////////////////////////////

static int  ix_lexer_peek_next_char     (ix_lexer* object);
static int  ix_lexer_read_next_char     (ix_lexer* object);
static void ix_lexer_process_begin      (ix_lexer* object, ix_token* result);
static void ix_lexer_process_end        (ix_lexer* object, ix_token* result);
static void ix_lexer_process_identifier (ix_lexer* object, ix_token* result);
static void ix_lexer_process_number     (ix_lexer* object, ix_token* result);
static void ix_lexer_process_string     (ix_lexer* object, ix_token* result);
static void ix_lexer_process_token      (ix_lexer* object, ix_token* result);
static void ix_lexer_parse_next_token   (ix_lexer* object, ix_token* result);

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region private implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// private implementation
//
//////////////////////////////////////////////////////////////////////////

int ix_lexer_peek_next_char(ix_lexer* object)
{
  int result = 0;
  
  if (!ix_stream_read(object->stream, &result, sizeof(uint8_t)))
  {
    return -1;
  }
  
  ix_stream_seek(object->stream, IXSS_CUR, -1);
  
  return result;
}

int ix_lexer_read_next_char(ix_lexer* object)
{
  int result = 0;

  if (!ix_stream_read(object->stream, &result, sizeof(uint8_t)))
  {
    result = -1;
  }

  if (result == '\n')
  {
    object->source_info.column = 1;
    object->source_info.line++;
  }
  else
  {
    object->source_info.column++;
  }

  return result;
}

void ix_lexer_process_begin(ix_lexer* object, ix_token* result)
{
  IX_UNUSED(object);

  result->token_type = IXT_BEGIN;
}

void ix_lexer_process_end(ix_lexer* object, ix_token* result)
{
  IX_UNUSED(object);

  result->token_type = IXT_END;
}

void ix_lexer_process_identifier(ix_lexer* object, ix_token* result)
{
  int c;
  size_t i = 0;

  result->token_type = IXT_IDENTIFIER;

  while ((c = ix_lexer_peek_next_char(object)))
  {
    if (isalnum(c) || c == '_')
    {
      result->token_value[i++] = ix_lexer_read_next_char(object);
    }
    else
    {
      result->token_value[i++] = '\0';
      break;
    }
  }

  for (i = 0; i < ix_token_keyword_table_size; i++)
  {
    if (!strcmp(result->token_value, ix_token_keyword_table[i].keyword_literal))
    {
      result->token_type = ix_token_keyword_table[i].token_type;
      return;
    }
  }
}

void ix_lexer_process_number(ix_lexer* object, ix_token* result)
{
  int c;
  size_t i = 0;

  result->token_type = IXT_INTEGER;

  while ((c = ix_lexer_peek_next_char(object)))
  {
    if (isdigit(c))
    {
      result->token_value[i++] = ix_lexer_read_next_char(object);
    }
    else if (c == '.')
    {
      // make more intelligent parsing
      result->token_type = IXT_REAL;
      result->token_value[i++] = ix_lexer_read_next_char(object);
    }
    else
    {
      result->token_value[i++] = '\0';
      break;
    }
  }
}

void ix_lexer_process_string(ix_lexer* object, ix_token* result)
{
  int c;
  size_t i= 0;

  IX_UNUSED(object);

  result->token_type = IXT_STRING;

  //
  // skip begin quotes
  //

  IX_UNUSED(ix_lexer_read_next_char(object));

  while ((c = ix_lexer_peek_next_char(object)) != '\"')
  {
    result->token_value[i++] = ix_lexer_read_next_char(object);
  }

  result->token_value[i++] = '\0';

  //
  // skip end quotes
  //

  IX_UNUSED(ix_lexer_read_next_char(object));
}

void ix_lexer_process_token(ix_lexer* object, ix_token* result)
{
  int c;
  size_t i;
  size_t ii = 0;

  ix_token_type current_state = IXT_NONE;

  result->token_type = IXT_INVALID;

  do
  {
    c = ix_lexer_peek_next_char(object);

    for (i = 0; i < ix_token_state_table_size; i++)
    {
      if (current_state == ix_token_state_table[i].current_state &&
          c == ix_token_state_table[i].character)
      {
        current_state = ix_token_state_table[i].next_state;
        result->token_type = ix_token_state_table[i].token_type;
        result->token_value[ii++] = ix_lexer_read_next_char(object);
        break;
      }
    }

    if (i == ix_token_state_table_size)
    {
      break;
    }
  } while (current_state != IXT_NONE);

  result->token_value[ii++] = '\0';
}

void ix_lexer_parse_next_token(ix_lexer* object, ix_token* result)
{
  int c;

  while (isspace(ix_lexer_peek_next_char(object)))
  {
    ix_lexer_read_next_char(object);
  }

  result->source_info.line = object->source_info.line;
  result->source_info.column = object->source_info.column;

  c = ix_lexer_peek_next_char(object);

  if (c == -1)                      ix_lexer_process_end(object, result);
  else if (isalpha(c) || c == '_')  ix_lexer_process_identifier(object, result);
  else if (isdigit(c))              ix_lexer_process_number(object, result);
  else if (c == '"')                ix_lexer_process_string(object, result);
  else                              ix_lexer_process_token(object, result);
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix_lexer* ix_lexer_ctor(ix_lexer* object, ix_error_list* errors)
{
  IX_OBJECT_SET_DTOR(object, &ix_lexer_dtor);

  object->stream = NULL;
  object->source_info.line = 1;
  object->source_info.column = 1;

  object->errors = errors;

  return object;
}

void ix_lexer_dtor(ix_object* object)
{
  ix_lexer* lexer = (ix_lexer*)object;

  IX_UNUSED(lexer);
}

void ix_lexer_process_stream(ix_lexer* object, ix_stream* stream, ix_token_list* token_list)
{
  ix_token token;

  object->stream = stream;

  ix_lexer_process_begin(object, &token);
  ix_list_add(token_list, &token);

  for (;;)
  {
    ix_lexer_parse_next_token(object, &token);

    if (token.token_type == IXT_INVALID)
    {
      ix_error_add(IXET_LEXER_UNEXPECTED_CHARACTER, token.source_info);

      //
      // skip next character
      //
      
      ix_lexer_read_next_char(object);
      continue;
    }

    ix_list_add(token_list, &token);

    if (token.token_type == IXT_END)
    {
      break;
    }
  }

  if (object->errors->size != 0)
  {
    ix_throw();
  }
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
