#include "vm.h"
#include "stream.h"
#include "mem_stream.h"

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>

#ifdef IX_COMPILER_MSVC
# pragma region function prototypes
#endif

//////////////////////////////////////////////////////////////////////////
//
// function prototypes
//
//////////////////////////////////////////////////////////////////////////

static void ix_variable_from_stream           (ix_variable* variable, ix_stream* stream);

static void ix_vm_parse_data                  (ix_vm* object, ix_stream* code_stream);
static void ix_vm_parse_code                  (ix_vm* object, ix_stream* code_stream);
static void ix_vm_handle_call                 (ix_vm* object);
static void ix_vm_handle_unary_operation      (ix_vm* object, ix_op opcode);
static void ix_vm_handle_binary_operation     (ix_vm* object, ix_op opcode);
static void ix_vm_handle_compare_operation    (ix_vm* object, ix_op opcode);
static void ix_vm_handle_arithmetic_operation (ix_vm* object, ix_op opcode);

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region private implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// private implementation
//
//////////////////////////////////////////////////////////////////////////

static void ix_variable_from_stream(ix_variable* variable, ix_stream* stream)
{
  ix_value* value = &variable->value;
  uint8_t   variable_name_size;

  ix_stream_read(stream, &variable_name_size, sizeof(variable_name_size));
  ix_stream_read(stream, variable->name, variable_name_size);

  variable->name[variable_name_size] = '\0';

  variable->name_hash = ix_string_hash(variable->name);

  ix_stream_read(stream, &value->type, sizeof(uint8_t));

  switch (value->type)
  {
    case IXVT_NULL:

      //
      // read nothing
      //

      break;

    case IXVT_FUNCTION:
      ix_stream_read(stream, &value->value.offset, sizeof(value->value.offset));
      break;

    case IXVT_INTEGER:
      ix_stream_read(stream, &value->value.integer, sizeof(value->value.integer));
      break;

    case IXVT_REAL:
      ix_stream_read(stream, &value->value.real, sizeof(value->value.real));
      break;

    case IXVT_STRING:
      {
        uint32_t string_size;

        ix_construct(ix_string, &value->value.string);

        ix_stream_read(stream, &string_size, sizeof(string_size));
        ix_string_reserve(&value->value.string, string_size);
        ix_stream_read(stream, value->value.string.content, string_size);
        value->value.string.size = string_size;
        value->value.string.content[string_size] = '\0';
      }
      break;

    default:
      IX_ASSERT(0);
      break;
  }
}

static void ix_vm_parse_data(ix_vm* object, ix_stream* code_stream)
{
  ix_variable variable;
  uint8_t read_byte;

  ix_construct(ix_variable, &variable);

  while (ix_stream_read(code_stream, &read_byte, sizeof(read_byte)) && read_byte)
  {
    ix_stream_seek(code_stream, IXSS_CUR, -1);

    ix_variable_from_stream(&variable, code_stream);
    ix_list_add(&object->vmctx.data, &variable);
  }
}

static void ix_vm_parse_code(ix_vm* object, ix_stream* code_stream)
{
  ix_stream_copy_from(&object->vmctx.code, code_stream);
}

static void ix_vm_handle_call(ix_vm* object)
{
  ix_variable* call_argument = (ix_variable*)ix_list_get(&object->vmctx.data, ix_vm_context_peek_next(&object->vmctx, ix_offset_type));

  if (call_argument->value.value.offset != -1)
  {
    ix_vm_context_call(&object->vmctx, call_argument->value.value.offset);
  }
  else
  {
    ix_value_list       args;
    ix_value            arg_count_var;
    ix_integer_type     arg_count;
    ix_extern_function* function;
    uint32_t            fn_name_hash = ix_string_hash(call_argument->name);

    ix_construct_list(ix_value, &args, IXLF_OBJECT);
    ix_vm_context_stack_pop(&object->vmctx, &arg_count_var);
    arg_count = arg_count_var.value.integer;

    while (arg_count--)
    {
      //
      // no need to construct or make deep-copy of ix_variable,
      // because the object will be definitely destroyed
      // at the end of this procedure
      //

      ix_value val;      
      ix_vm_context_stack_pop(&object->vmctx, &val);

      ix_list_add(&args, &val);
    }

    ix_list_foreach(function, &object->extern_functions)
    {
      if (function->name_hash == fn_name_hash)
      {
        function->function(&args);
        break;
      }
    }

    ix_destroy(&args);

    object->vmctx.ip += sizeof(ix_offset_type) + 1;
  }
}

static void ix_vm_handle_unary_operation(ix_vm* object, ix_op opcode)
{
  ix_value v;
  ix_value result;

  ix_construct(ix_value, &result);
  ix_vm_context_stack_pop(&object->vmctx, &v);

  if (opcode == IXOP_NOT && v.type != IXVT_INTEGER)
  {
    //throw std::runtime_error("invalid binary operation");
  }

  switch (opcode)
  {
    case IXOP_NOT:
      ix_value_integer(&result, ~v.value.integer);
      ix_vm_context_stack_push(&object->vmctx, &result);
      break;

//     case IXOP_UNM:
//       if (v.type == IXVT_INTEGER)
//       {
//         ix_value_integer(&result, -v.value.integer);
//         ix_vm_context_stack_push(&object->vmctx, &result);
//       }
//       else if (v.type == IXVT_REAL)
//       {
//         ix_value_real(&result, -v.value.real);
//         ix_vm_context_stack_push(&object->vmctx, &result);
//       }
//       break;

    default:
      IX_ASSERT(0);
      break;
  }

  ix_destroy(&v);
}

static void ix_vm_handle_binary_operation(ix_vm* object, ix_op opcode)
{
  ix_value v1;
  ix_value v2;
  ix_value result;

  ix_construct(ix_value, &result);
  ix_vm_context_stack_pop(&object->vmctx, &v1);
  ix_vm_context_stack_pop(&object->vmctx, &v2);

  if (v1.type != IXVT_INTEGER || v2.type != IXVT_INTEGER)
  {
    //throw std::runtime_error("invalid binary operation");
  }

#define IX_VM_BINOP(opcode, op)                                                         \
  case opcode:                                                                          \
    ix_value_integer(&result, (ix_integer_type)(v1.value.integer op v2.value.integer)); \
    ix_vm_context_stack_push(&object->vmctx, &result);                                  \
    break

  switch (opcode)
  {
    IX_VM_BINOP(IXOP_AND, &&);
    IX_VM_BINOP(IXOP_OR,  ||);
    IX_VM_BINOP(IXOP_XOR, ^);
    default: IX_ASSERT(0); break;
  }

#undef IX_VM_BINOP

  ix_destroy(&v2);
  ix_destroy(&v1);
}

static void ix_vm_handle_compare_operation(ix_vm* object, ix_op opcode)
{
  ix_value v1;
  ix_value v2;
  ix_value result;

  ix_construct(ix_value, &result);
  ix_vm_context_stack_pop(&object->vmctx, &v1);
  ix_vm_context_stack_pop(&object->vmctx, &v2);

#define IX_VM_CMPOP(opcode, op)                                                             \
  case opcode:                                                                              \
    {                                                                                       \
      if (v1.type == IXVT_INTEGER &&                                                        \
          v2.type == IXVT_INTEGER)                                                          \
      {                                                                                     \
        ix_value_integer(&result, (ix_integer_type)(v1.value.integer op v2.value.integer)); \
        ix_vm_context_stack_push(&object->vmctx, &result);                                  \
      }                                                                                     \
      else if (v1.type == IXVT_REAL &&                                                      \
               v2.type == IXVT_REAL)                                                        \
      {                                                                                     \
        ix_value_integer(&result, (ix_integer_type)(v1.value.real op v2.value.real));       \
        ix_vm_context_stack_push(&object->vmctx, &result);                                  \
      }                                                                                     \
      else if (v1.type == IXVT_INTEGER &&                                                   \
               v2.type == IXVT_REAL)                                                        \
      {                                                                                     \
        ix_value_integer(&result, (ix_integer_type)(v1.value.integer op v2.value.real));    \
        ix_vm_context_stack_push(&object->vmctx, &result);                                  \
      }                                                                                     \
      else if (v1.type == IXVT_REAL &&                                                      \
               v2.type == IXVT_INTEGER)                                                     \
      {                                                                                     \
        ix_value_integer(&result, (ix_integer_type)(v1.value.real op v2.value.integer));    \
        ix_vm_context_stack_push(&object->vmctx, &result);                                  \
      }                                                                                     \
      else                                                                                  \
      {                                                                                     \
        /* throw std::runtime_error("invalid math operation");  */                          \
      }                                                                                     \
    }                                                                                       \
    break

  switch (opcode)
  {
    IX_VM_CMPOP(IXOP_EQ,  ==);
    IX_VM_CMPOP(IXOP_NEQ, !=);
    IX_VM_CMPOP(IXOP_LT,  < );
    IX_VM_CMPOP(IXOP_LTE, <=);
    IX_VM_CMPOP(IXOP_GT,  > );
    IX_VM_CMPOP(IXOP_GTE, >=);
    default: IX_ASSERT(0); break;
  }

#undef IX_VM_CMPOP

  ix_destroy(&v2);
  ix_destroy(&v1);
}

static void ix_vm_handle_arithmetic_operation(ix_vm* object, ix_op opcode)
{
  ix_value v1;
  ix_value v2;
  ix_value result;

  ix_construct(ix_value, &result);
  ix_vm_context_stack_pop(&object->vmctx, &v1);
  ix_vm_context_stack_pop(&object->vmctx, &v2);

  do 
  {
    //
    // special case #1
    // we can do modulo only on two integers:
    // integer % integer : integer
    // modulo on anything else is error
    //

    if (opcode == IXOP_MOD)
    {
      if (v1.type == IXVT_INTEGER && v2.type == IXVT_INTEGER)
      {
        ix_value_integer(&result, v1.value.integer % v2.value.integer);
        ix_vm_context_stack_push(&object->vmctx, &result);
      }
      else
      {
        //throw std::runtime_error("division by zero");
      }

      break;
    }

    //
    // special case #2
    // we cant do division by zero
    //

    else if (opcode == IXOP_DIV)
    {
      if ( (v2.type == IXVT_INTEGER && v2.value.integer == 0)
        || (v2.type == IXVT_REAL    && fabs(v2.value.real) == 0.0))
      {
        //throw std::runtime_error("division by zero");
      }
    }

    //
    // special case #3
    // string concatenation:
    // string + string : string
    //

    else if (v1.type == IXVT_STRING &&
             v2.type == IXVT_STRING &&
             opcode  == IXOP_ADD)
    {
      ix_value_string(&result, &v1.value.string);
      ix_string_append_string(&result.value.string, &v2.value.string);
      ix_vm_context_stack_push(&object->vmctx, &result);
    
      break;
    }

    //
    // handle "normal" number operation
    //

#define IX_VM_MATHOP(opcode, op)                                                          \
  case opcode:                                                                            \
    if (v1.type == IXVT_INTEGER &&                                                        \
        v2.type == IXVT_INTEGER)                                                          \
    {                                                                                     \
      ix_value_integer(&result, (ix_integer_type)(v1.value.integer op v2.value.integer)); \
      ix_vm_context_stack_push(&object->vmctx, &result);                                  \
    }                                                                                     \
    else if (v1.type == IXVT_REAL &&                                                      \
             v2.type == IXVT_REAL)                                                        \
    {                                                                                     \
      ix_value_real(&result, (ix_real_type)(v1.value.real op v2.value.real));             \
      ix_vm_context_stack_push(&object->vmctx, &result);                                  \
    }                                                                                     \
    else if (v1.type == IXVT_INTEGER &&                                                   \
             v2.type == IXVT_REAL)                                                        \
    {                                                                                     \
      ix_value_real(&result, (ix_real_type)(v1.value.integer op v2.value.real));          \
      ix_vm_context_stack_push(&object->vmctx, &result);                                  \
    }                                                                                     \
    else if (v1.type == IXVT_REAL &&                                                      \
             v2.type == IXVT_INTEGER)                                                     \
    {                                                                                     \
      ix_value_real(&result, (ix_real_type)(v1.value.real op v2.value.integer));          \
      ix_vm_context_stack_push(&object->vmctx, &result);                                  \
    }                                                                                     \
    else                                                                                  \
    {                                                                                     \
      /* throw std::runtime_error("invalid math operation");  */                          \
    }                                                                                     \
    break

    switch (opcode)
    {
      IX_VM_MATHOP(IXOP_ADD, +);
      IX_VM_MATHOP(IXOP_SUB, -);
      IX_VM_MATHOP(IXOP_MUL, *);
      IX_VM_MATHOP(IXOP_DIV, /);
      default: IX_ASSERT(0); break;
    }

#undef IX_VM_MATHOP

  } while (0);

  ix_destroy(&v2);
  ix_destroy(&v1);
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix_vm* ix_vm_ctor(ix_vm* object, ix_error_list* errors)
{
  IX_OBJECT_SET_DTOR(object, &ix_vm_dtor);

  ix_construct(ix_vm_context, &object->vmctx);
  ix_construct_list(ix_extern_function, &object->extern_functions, IXLF_NONE);

  object->errors = errors;

  return object;
}

void ix_vm_dtor(ix_object* object)
{
  ix_vm* vm = (ix_vm*)object;

  ix_destroy(&vm->extern_functions);
  ix_destroy(&vm->vmctx);
}

void ix_vm_parse_bytecode(ix_vm* object, ix_stream* code_stream)
{
  ix_vm_parse_data(object, code_stream);
  ix_vm_parse_code(object, code_stream);
}

void ix_vm_register_function(ix_vm* object, const char* function_name, ix_extern_callback* callback)
{
  ix_extern_function extern_function = {
    function_name,
    ix_string_hash(function_name),
    callback
  };

  ix_list_add(&object->extern_functions, &extern_function);
}

void ix_vm_run(ix_vm* object)
{
  bool running = true;

  ix_value value;
  ix_construct(ix_value, &value);

  ix_vm_context_stack_push(&object->vmctx, &value);
  
  while (running)
  {
    ix_op current_instruction = ix_vm_context_peek_instruction(&object->vmctx);

    value.type = IXVT_NULL;

    switch (current_instruction)
    {
      case IXOP_HLT:
        running = false;
        continue;

      case IXOP_PUSHI:
        ix_value_integer(&value, ix_vm_context_read_next(&object->vmctx, ix_integer_type));
        ix_vm_context_stack_push(&object->vmctx, &value);
        break;

      case IXOP_PUSHR:
        ix_value_real(&value, ix_vm_context_read_next(&object->vmctx, ix_real_type));
        ix_vm_context_stack_push(&object->vmctx, &value);
        break;

      case IXOP_POP:
        ix_vm_context_stack_pop(&object->vmctx, &value);
        ix_destroy(&value);
        break;

      case IXOP_LD:
        ix_value_copy_from(
          &value,
          &((ix_variable*)ix_list_get(&object->vmctx.data, ix_vm_context_read_next(&object->vmctx, ix_offset_type)))->value
        );

        ix_vm_context_stack_push(&object->vmctx, &value);
        break;

      case IXOP_ST:
        ix_value_copy_from(
          &((ix_variable*)ix_list_get(&object->vmctx.data, ix_vm_context_read_next(&object->vmctx, ix_offset_type)))->value,
          ix_vm_context_stack_top(&object->vmctx)
        );
        break;

      case IXOP_STP:
        ix_list_pop_copy(
          &object->vmctx.stack,
          &((ix_variable*)ix_list_get(&object->vmctx.data, ix_vm_context_read_next(&object->vmctx, ix_offset_type)))->value
        );
        break;

      case IXOP_JMP:
        ix_vm_context_jmp(&object->vmctx, ix_vm_context_read_next(&object->vmctx, ix_offset_type));
        continue;

      case IXOP_JMPT:
        // int check?
        ix_vm_context_stack_pop(&object->vmctx, &value);
        if (value.value.integer)
        {
          ix_vm_context_jmp(&object->vmctx, ix_vm_context_read_next(&object->vmctx, ix_offset_type));
          continue;
        }
        
        // jump over offset
        ix_vm_context_jmp(&object->vmctx, object->vmctx.ip + sizeof(ix_offset_type));
        break;

      case IXOP_JMPNT:
        ix_vm_context_stack_pop(&object->vmctx, &value);
        if (!value.value.integer)
        {
          ix_vm_context_jmp(&object->vmctx, ix_vm_context_read_next(&object->vmctx, ix_offset_type));
          continue;
        }

        // jump over offset
        ix_vm_context_jmp(&object->vmctx, object->vmctx.ip + sizeof(ix_offset_type));
        break;

      case IXOP_CALL:
        ix_vm_handle_call(object);
        continue;

      case IXOP_RET:
        if (!ix_vm_context_return(&object->vmctx))
        {
          running = false;
          IX_ASSERT(object->vmctx.stack.size == 1);
          continue;
        }
        break;

      case IXOP_AND:
      case IXOP_OR:
      case IXOP_XOR:
        ix_vm_handle_binary_operation(object, current_instruction);
        break;

      case IXOP_NOT:
//      case IXOP_UNM:
        ix_vm_handle_unary_operation(object, current_instruction);
        break;

      case IXOP_ADD:
      case IXOP_SUB:
      case IXOP_MUL:
      case IXOP_DIV:
        ix_vm_handle_arithmetic_operation(object, current_instruction);
        break;

      case IXOP_EQ:
      case IXOP_NEQ:
      case IXOP_LT:
      case IXOP_LTE:
      case IXOP_GT:
      case IXOP_GTE:
        ix_vm_handle_compare_operation(object, current_instruction);
        break;

//       case IXOP_SSET:
//         {
//           variable v1 = _vm.stack_pop();
//           variable v2 = _vm.stack_pop();
// 
//           v1.value.string->assign(*v2.value.string);
//         }
//         break;
// 
//       case IXOP_SCONCAT:
//         {
//           variable v1 = _vm.stack_pop();
//           variable v2 = _vm.stack_pop();
//           variable v3 = _vm.stack_pop();
// 
//           v3.value.string->assign(*v1.value.string + *v2.value.string);
//         }
//         break;
// 
//       case IXOP_SSUBSTR:
//         {
//           variable v1 = _vm.stack_pop();
//           variable v2 = _vm.stack_pop();
//           variable v3 = _vm.stack_pop();
//           variable v4 = _vm.stack_pop();
// 
//           v4.value.string->assign(
//             v3.value.string->substr(
//               static_cast<std::string::size_type>(v2.value.integer),
//               static_cast<std::string::size_type>(v1.value.integer)));
//         }
//         break;
// 
//       case IXOP_SSIZE:
//         _vm.stack_push(integer_type(_vm.stack_pop().value.string->size()));
//         break;

      default:
        IX_ASSERT(0);
        break;
    }

    object->vmctx.ip++;
  }
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
