#include "file_stream.h"
#include "types.h"

#include <stdio.h>

#ifdef IX_COMPILER_MSVC
# pragma region function prototypes
#endif

//////////////////////////////////////////////////////////////////////////
//
// function prototypes
//
//////////////////////////////////////////////////////////////////////////

static size_t ix_file_stream_read     (void* object, void* buffer, size_t size);
static size_t ix_file_stream_write    (void* object, const void* buffer, size_t size);
static bool   ix_file_stream_seek     (void* object, ix_stream_origin origin, long offset);
static size_t ix_file_stream_resize   (void* object, size_t new_size);
static size_t ix_file_stream_size     (void* object);
static size_t ix_file_stream_position (void* object);
static void   ix_file_stream_close    (void* object);

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region private implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// private implementation
//
//////////////////////////////////////////////////////////////////////////

size_t ix_file_stream_read(void* object, void* buffer, size_t size)
{
  return fread(buffer, 1, size, ((ix_file_stream*)object)->file);
}

size_t ix_file_stream_write(void* object, const void* buffer, size_t size)
{
  return fwrite(buffer, 1, size, ((ix_file_stream*)object)->file);
}

bool ix_file_stream_seek(void* object, ix_stream_origin origin, long offset)
{
  return !fseek(((ix_file_stream*)object)->file, offset, origin);
}

size_t ix_file_stream_resize(void* object, size_t new_size)
{
  IX_UNUSED(object);
  IX_UNUSED(new_size);

  IX_ASSERT(0 && "not implemented");
  return 0;
}

size_t ix_file_stream_size(void* object)
{
  IX_UNUSED(object);

  IX_ASSERT(0 && "not implemented");
  return 0;
}

size_t ix_file_stream_position(void* object)
{
  return (size_t)ftell(((ix_file_stream*) object)->file);
}

void ix_file_stream_close(void* object)
{
  if (((ix_file_stream*)object)->file)
  {
    fclose(((ix_file_stream*)object)->file);
  }
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix_file_stream* ix_file_stream_ctor(ix_file_stream* object)
{
  IX_OBJECT_SET_DTOR(object, &ix_file_stream_dtor);

  object->base.read     = &ix_file_stream_read;
  object->base.write    = &ix_file_stream_write;
  object->base.seek     = &ix_file_stream_seek;
  object->base.resize   = &ix_file_stream_resize;
  object->base.size     = &ix_file_stream_size;
  object->base.position = &ix_file_stream_position;
  object->base.close    = &ix_file_stream_close;
  object->open          = &ix_file_stream_open;
  object->file = NULL;

  return object;
}

void ix_file_stream_dtor(ix_object* object)
{
  ((ix_file_stream*)object)->base.close(object);
}

bool ix_file_stream_open(void* object, const char* filename, const char* mode)
{
  return !!((((ix_file_stream*)object)->file) = fopen(filename, mode));
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
