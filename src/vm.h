#pragma once
#ifndef IX_VM_H
#define IX_VM_H

#include "vm_context.h"
#include "variable.h"
#include "list.h"
#include "error.h"
#include "types.h"

#include <stdint.h>
#include <stddef.h>

typedef int(ix_extern_callback)(ix_variable_list*);

typedef struct ix_tag_extern_function
{
  const char*         name;
  uint32_t            name_hash;
  ix_extern_callback* function;
} ix_extern_function;

typedef ix_list ix_extern_function_list;

typedef struct ix_tag_vm
{
  IX_OBJECT;

  ix_error_list*  errors;

  ix_extern_function_list extern_functions;
  ix_vm_context   vmctx;
} ix_vm;

ix_vm*  ix_vm_ctor              (ix_vm* object, ix_error_list* errors);
void    ix_vm_dtor              (ix_object* object);
void    ix_vm_parse_bytecode    (ix_vm* object, ix_stream* code_stream);
void    ix_vm_register_function (ix_vm* object, const char* function_name, ix_extern_callback* callback);
void    ix_vm_run               (ix_vm* object);

#endif
