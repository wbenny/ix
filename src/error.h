#pragma once
#ifndef IX_ERROR_H
#define IX_ERROR_H

#include "token.h"
#include "list.h"
#include "types.h"

#include <setjmp.h>

extern jmp_buf ix_exception_buf;

typedef enum ix_tag_error_type
{
  IXET_NONE             = 0x0000,

  //
  // this style leaves us
  // 255 possible error codes for each component
  //

  IXET_LEXER            = 0x0100,
  IXET_PARSER           = 0x0200,
  IXET_SEMANTIC         = 0x0400,
  IXET_COMPILER         = 0x0800,

  IXET_SEVERITY_WARNING = 0x1000,
  IXET_SEVERITY_ERROR   = 0x2000,
  IXET_SEVERITY_FATAL   = 0x4000,

  IXET_LEXER_UNEXPECTED_CHARACTER   = IXET_LEXER  | IXET_SEVERITY_FATAL | 1,

  IXET_PARSER_INTERNAL_ERROR        = IXET_PARSER | IXET_SEVERITY_FATAL | 1,
  IXET_PARSER_UNEXPECTED_TOKEN      = IXET_PARSER | IXET_SEVERITY_ERROR | 2,
  IXET_PARSER_TOKEN_IS_NOT_TERMINAL = IXET_PARSER | IXET_SEVERITY_ERROR | 3,

} ix_error_type;

typedef struct ix_tag_error
{
  ix_error_type  error_type;
  ix_source_info source_info;

  union
  {
    void* any;

    union
    {
      char unexpected_character;
    } lexer;

    union 
    {
      ix_token* unexpected_token;
      ix_token* non_terminal_token;
    } parser;
  } type;
} ix_error;

typedef ix_list ix_error_list;

#define ix_try                            if (!setjmp(ix_exception_buf))
#define ix_catch                          else
#define ix_throw()                        longjmp(ix_exception_buf, 1)

#if defined(IX_COMPILER_MSVC) && defined (_DEBUG) && 0
# define IX_DEBUG_BREAK                   __debugbreak()
#else
# define IX_DEBUG_BREAK
#endif

#define ix_error_add_ex(err_type, src_info, err_info) \
      do {                                            \
        ix_error err;                                 \
                                                      \
        err.error_type = (err_type);                  \
        err.source_info = (src_info);                 \
        err.type.any = (void*)(err_info);             \
        ix_list_add(object->errors, &err);            \
                                                      \
        IX_DEBUG_BREAK;                               \
                                                      \
        if ((err_type) & IXET_SEVERITY_FATAL)         \
          ix_throw();                                 \
                                                      \
      } while (0)

#define ix_error_add(err_type, src_info)              \
      ix_error_add_ex(err_type, src_info, NULL)

#endif
