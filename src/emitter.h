#pragma once
#ifndef IX_EMITTER_H
#define IX_EMITTER_H

#include "list.h"
#include "variable.h"
#include "stream.h"
#include "opcodes.h"
#include "types.h"

#include <stdint.h>

typedef ix_list ix_code;
typedef ix_list ix_jmp_table_map;

typedef struct ix_tag_emitter
{
  IX_OBJECT;

  ix_stream*  code_stream;
  size_t      code_section;

  ix_variable_list variable_list;
  ix_jmp_table_map jmp_table;
  ix_jmp_table_map jmp_table_reverse;
} ix_emitter;

typedef enum ix_tag_vm_argument_type
{
  IXVMAT_NONE,
  IXVMAT_INTEGER,
  IXVMAT_REAL,
  IXVMAT_LABEL,
  IXVMAT_JUMP,
} ix_vm_argument_type;

ix_emitter* ix_emitter_ctor                     (ix_emitter* object, ix_stream* code_stream);
void        ix_emitter_dtor                     (ix_object* object);

void        ix_emitter_emit                     (ix_emitter* object, ix_op opcode, ix_vm_argument_type arg_type, const void* arg);
void        ix_emitter_emit_label               (ix_emitter* object, const char* label);
void        ix_emitter_emit_variable            (ix_emitter* object, ix_variable* variable);
void        ix_emitter_mark_end_of_data_section (ix_emitter* object);
void        ix_emitter_fix_jumps                (ix_emitter* object);

#endif
