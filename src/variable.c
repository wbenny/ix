#include "variable.h"

#include <string.h>

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

#ifdef IX_COMPILER_MSVC
# pragma region value implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// value implementation
//
//////////////////////////////////////////////////////////////////////////

ix_value* ix_value_ctor(ix_value* object)
{
  IX_OBJECT_SET_DTOR(object, &ix_value_dtor);

  return object;
}

void ix_value_dtor(ix_object* object)
{
  ix_value* value = (ix_value*)object;

  if (value->type == IXVT_STRING)
  {
    ix_destroy(&value->value.string);
  }
}

void ix_value_copy_from(ix_value* object, ix_value* source)
{
  if (object->type == IXVT_STRING)
  {
    ix_destroy(&object->value.string);
  }

  object->type = source->type;

  if (source->type == IXVT_STRING)
  {
    ix_construct(ix_string, &object->value.string);
    ix_string_copy_from(&object->value.string, &source->value.string);
  }
  else
  {
    object->value = source->value;
  }
}

void ix_value_null(ix_value* object)
{
  object->type = IXVT_NULL;
}

void ix_value_offset(ix_value* object, ix_offset_type offset)
{
  object->type = IXVT_FUNCTION;
  object->value.offset = offset;
}

void ix_value_integer(ix_value* object, ix_integer_type integer)
{
  object->type = IXVT_INTEGER;
  object->value.integer = integer;
}

void ix_value_real(ix_value* object, ix_real_type real)
{
  object->type = IXVT_REAL;
  object->value.real = real;
}

void ix_value_string(ix_value* object, ix_string* string)
{
  object->type = IXVT_STRING;
  ix_construct(ix_string, &object->value.string);
  ix_string_set_string(&object->value.string, string);
}

void ix_value_char_array(ix_value* object, const char* string)
{
  object->type = IXVT_STRING;
  ix_construct(ix_string, &object->value.string);
  ix_string_set_array(&object->value.string, string, strlen(string));
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif


#ifdef IX_COMPILER_MSVC
# pragma region variable implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// variable implementation
//
//////////////////////////////////////////////////////////////////////////

ix_variable* ix_variable_ctor(ix_variable* object)
{
  IX_OBJECT_SET_DTOR(object, &ix_variable_dtor);

  ix_construct(ix_value, &object->value);

  return object;
}

void ix_variable_dtor(ix_object* object)
{
  ix_variable* variable = (ix_variable*)object;

  ix_destroy(&variable->value);
}

void ix_variable_copy_from(ix_variable* object, ix_variable* source)
{
  ix_variable_set_name(object, source->name);
  ix_value_copy_from(&object->value, &source->value);
}

void ix_variable_set_name(ix_variable* object, const char* name)
{
  strcpy(object->name, name);
  object->name_hash = ix_string_hash(name);
}

void ix_variable_set_value(ix_variable* object, ix_value* value)
{
  ix_value_copy_from(&object->value, value);
}

void ix_variable_null(ix_variable* object)
{
  ix_value_null(&object->value);
}

void ix_variable_offset(ix_variable* object, ix_offset_type offset)
{
  ix_value_offset(&object->value, offset);
}

void ix_variable_integer(ix_variable* object, ix_integer_type integer)
{
  ix_value_integer(&object->value, integer);
}

void ix_variable_real(ix_variable* object, ix_real_type real)
{
  ix_value_real(&object->value, real);
}

void ix_variable_string(ix_variable* object, ix_string* string)
{
  ix_value_string(&object->value, string);
}

void ix_variable_char_array(ix_variable* object, const char* string)
{
  ix_value_char_array(&object->value, string);
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
