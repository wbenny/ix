#pragma once
#ifndef IX_COMPILER_H
#define IX_COMPILER_H

#include "ast.h"
#include "symbol.h"
#include "stream.h"
#include "emitter.h"
#include "error.h"
#include "types.h"

#include <stddef.h>

typedef struct ix_tag_compiler
{
  IX_OBJECT;

  ix_error_list*  errors;

  ix_emitter      emitter;

  uint32_t        label_id;
  size_t          expression_level;
} ix_compiler;

ix_compiler*  ix_compiler_ctor    (ix_compiler* object, ix_error_list* errors, ix_stream* code_stream);
void          ix_compiler_dtor    (ix_object* object);

void          ix_compiler_compile (ix_compiler* object, ix_ast* ast, ix_symbol_table* symbol_table);

#endif
