#include "parser.h"

#include <string.h>
#include <stdarg.h>

#ifdef IX_COMPILER_MSVC
# pragma region private structs
#endif

//////////////////////////////////////////////////////////////////////////
//
// private structs
//
//////////////////////////////////////////////////////////////////////////

//
// operation category
// used for determining whether operator is unary/binary
//

typedef enum ix_tag_operation_category
{

  //
  // after category with this flag the expression can end
  // otherwise it's error (ie. "5 +" is syntax error)
  //
  
  IXTOC_TERMINAL_FLAG = 0x1000,

  IXTOC_NONE          = 1 | IXTOC_TERMINAL_FLAG,
  IXTOC_IDENTIFIER    = 2 | IXTOC_TERMINAL_FLAG,
  IXTOC_UNARY_PREFIX  = 3,
  IXTOC_UNARY_POSTFIX = 4 | IXTOC_TERMINAL_FLAG,
  IXTOC_BINARY        = 5,
} ix_operation_category;

typedef enum ix_tag_operation_associativity
{
  IXTOA_LEFT,
  IXTOA_RIGHT,
} ix_operation_associativity;

typedef struct ix_tag_operation
{
  ix_token_type               token_type;
  ix_operation_category       category;
  ix_operation_associativity  associativity;
  int                         precedence;
} ix_operation;

typedef struct ix_tag_operation_state
{
  ix_operation_category current_state;
  ix_operation_category next_state;
} ix_operation_state;

typedef struct ix_tag_token_operation_pair
{
  ix_token*     token;
  ix_operation* operation;
} ix_token_operation_pair;

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region private tables
#endif

//////////////////////////////////////////////////////////////////////////
//
// private tables
//
//////////////////////////////////////////////////////////////////////////

static ix_operation ix_operation_map[] =
{
  { IXT_COMMA,                  IXTOC_BINARY,         IXTOA_LEFT,   0 }, // ,

  { IXT_ASSIGN,                 IXTOC_BINARY,         IXTOA_RIGHT,  1 }, // =
  { IXT_PLUS_ASSIGN,            IXTOC_BINARY,         IXTOA_RIGHT,  1 }, // +=
  { IXT_MINUS_ASSIGN,           IXTOC_BINARY,         IXTOA_RIGHT,  1 }, // -=
  { IXT_MULTIPLY_ASSIGN,        IXTOC_BINARY,         IXTOA_RIGHT,  1 }, // *=
  { IXT_DIVIDE_ASSIGN,          IXTOC_BINARY,         IXTOA_RIGHT,  1 }, // /=
  { IXT_MODULO_ASSIGN,          IXTOC_BINARY,         IXTOA_RIGHT,  1 }, // %=
  { IXT_BITWISE_AND_ASSIGN,     IXTOC_BINARY,         IXTOA_RIGHT,  1 }, // &=
  { IXT_BITWISE_OR_ASSIGN,      IXTOC_BINARY,         IXTOA_RIGHT,  1 }, // |=
  { IXT_BITWISE_XOR_ASSIGN,     IXTOC_BINARY,         IXTOA_RIGHT,  1 }, // ^=
  { IXT_BIT_SHIFT_LEFT_ASSIGN,  IXTOC_BINARY,         IXTOA_RIGHT,  1 }, // <<=
  { IXT_BIT_SHIFT_RIGHT_ASSIGN, IXTOC_BINARY,         IXTOA_RIGHT,  1 }, // >>=

  { IXT_LOGICAL_OR,             IXTOC_BINARY,         IXTOA_LEFT,   2 }, // ||
  { IXT_LOGICAL_AND,            IXTOC_BINARY,         IXTOA_LEFT,   3 }, // &&
  { IXT_BITWISE_OR,             IXTOC_BINARY,         IXTOA_LEFT,   4 }, // |
  { IXT_BITWISE_XOR,            IXTOC_BINARY,         IXTOA_LEFT,   5 }, // ^
  { IXT_BITWISE_AND,            IXTOC_BINARY,         IXTOA_LEFT,   6 }, // &

  { IXT_EQUAL,                  IXTOC_BINARY,         IXTOA_LEFT,   7 }, // ==
  { IXT_NOT_EQUAL,              IXTOC_BINARY,         IXTOA_LEFT,   7 }, // !=

  { IXT_LESS_THAN,              IXTOC_BINARY,         IXTOA_LEFT,   8 }, // <
  { IXT_GREATER_THAN,           IXTOC_BINARY,         IXTOA_LEFT,   8 }, // >
  { IXT_LESS_THAN_OR_EQUAL,     IXTOC_BINARY,         IXTOA_LEFT,   8 }, // <=
  { IXT_GREATER_THAN_OR_EQUAL,  IXTOC_BINARY,         IXTOA_LEFT,   8 }, // >=

  { IXT_BIT_SHIFT_LEFT,         IXTOC_BINARY,         IXTOA_LEFT,   9 }, // <<
  { IXT_BIT_SHIFT_RIGHT,        IXTOC_BINARY,         IXTOA_LEFT,   9 }, // >>

  { IXT_PLUS,                   IXTOC_BINARY,         IXTOA_LEFT,  10 }, // +
  { IXT_MINUS,                  IXTOC_BINARY,         IXTOA_LEFT,  10 }, // -
  { IXT_MULTIPLY,               IXTOC_BINARY,         IXTOA_LEFT,  11 }, // *
  { IXT_DIVIDE,                 IXTOC_BINARY,         IXTOA_LEFT,  11 }, // /
  { IXT_MODULO,                 IXTOC_BINARY,         IXTOA_LEFT,  11 }, // %

//  { IXT_PLUS,                   IXTOC_UNARY_PREFIX,   IXTOA_RIGHT, 12 }, // +var
//  { IXT_MINUS,                  IXTOC_UNARY_PREFIX,   IXTOA_RIGHT, 12 }, // -var
//  { IXT_NOT,                    IXTOC_UNARY_PREFIX,   IXTOA_RIGHT, 12 }, // !var
//  { IXT_BITWISE_NOT,            IXTOC_UNARY_PREFIX,   IXTOA_RIGHT, 12 }, // ~var
//
//  { IXT_INCREMENT,              IXTOC_UNARY_PREFIX,   IXTOA_RIGHT, 12 }, // ++var
//  { IXT_DECREMENT,              IXTOC_UNARY_PREFIX,   IXTOA_RIGHT, 12 }, // --var

  { IXT_INCREMENT,              IXTOC_UNARY_POSTFIX,  IXTOA_LEFT,  13 }, // var++
  { IXT_DECREMENT,              IXTOC_UNARY_POSTFIX,  IXTOA_LEFT,  13 }, // var--
};

static ix_operation_state ix_operation_state_pairs[] = 
{
  { IXTOC_NONE,           IXTOC_NONE          },
  { IXTOC_NONE,           IXTOC_IDENTIFIER    },
  { IXTOC_NONE,           IXTOC_UNARY_PREFIX  },

  { IXTOC_IDENTIFIER,     IXTOC_BINARY        },
  { IXTOC_IDENTIFIER,     IXTOC_UNARY_POSTFIX },
  { IXTOC_IDENTIFIER,     IXTOC_NONE          },

  { IXTOC_BINARY,         IXTOC_UNARY_PREFIX  },
  { IXTOC_BINARY,         IXTOC_IDENTIFIER    },

  { IXTOC_UNARY_POSTFIX,  IXTOC_BINARY        },
  { IXTOC_UNARY_POSTFIX,  IXTOC_NONE          },

  { IXTOC_UNARY_PREFIX,   IXTOC_IDENTIFIER    },
};

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region function prototypes
#endif

//////////////////////////////////////////////////////////////////////////
//
// function prototypes
//
//////////////////////////////////////////////////////////////////////////

static ix_operation*      ix_operation_find                               (ix_token_type token_type);
static bool               ix_operation_state_exist                        (ix_operation_category current, ix_operation_category next);

static void               ix_ast_expression_ptr_list_add_node_unary       (ix_ast_expression_ptr_list* operand_stack, ix_token_operation_pair* op, ix_ast_expression* e);
static void               ix_ast_expression_ptr_list_add_node_binary      (ix_ast_expression_ptr_list* operand_stack, ix_token_operation_pair* op, ix_ast_expression* e);
static void               ix_ast_expression_ptr_list_add_node             (ix_ast_expression_ptr_list* operand_stack, ix_token_operation_pair* op);

static void*              ix_parser_check_token                           (ix_parser* object, ix_token* token, ...);

static ix_ast_identifier* ix_parser_parse_identifier                      (ix_parser* object);
static ix_ast_expression* ix_parser_parse_expression                      (ix_parser* object);
static ix_ast_expression* ix_parser_parse_expression_value                (ix_parser* object);
static ix_ast_expression* ix_parser_parse_expression_value_variable       (ix_parser* object);
static ix_ast_expression* ix_parser_parse_expression_value_string         (ix_parser* object);
static ix_ast_expression* ix_parser_parse_expression_value_number_integer (ix_parser* object);
static ix_ast_expression* ix_parser_parse_expression_value_number_real    (ix_parser* object);
static ix_ast_expression* ix_parser_parse_statement_block                 (ix_parser* object);
static ix_ast_expression* ix_parser_parse_statement_for                   (ix_parser* object);
static ix_ast_expression* ix_parser_parse_statement_while                 (ix_parser* object);
static ix_ast_expression* ix_parser_parse_statement_do_while              (ix_parser* object);
static ix_ast_expression* ix_parser_parse_statement_if                    (ix_parser* object);
static ix_ast_expression* ix_parser_parse_statement_last                  (ix_parser* object);
static ix_ast_expression* ix_parser_parse_statement_function              (ix_parser* object);
static ix_ast_expression* ix_parser_parse_statement_call                  (ix_parser* object);
static ix_ast_expression* ix_parser_parse_statement_declaration           (ix_parser* object);
static ix_ast_program*    ix_parser_parse_program                         (ix_parser* object);

#define ix_peek_token()         ((ix_token*)ix_list_get(object->parser_context.token_list, object->parser_context.position))
#define ix_peek_next_token()    ((ix_token*)ix_list_get(object->parser_context.token_list, object->parser_context.position + 1))
#define ix_peek_prev_token()    ((ix_token*)ix_list_get(object->parser_context.token_list, object->parser_context.position - 1))
#define ix_get_next_token()     ((ix_token*)ix_list_get(object->parser_context.token_list, ++object->parser_context.position))
#define ix_get_prev_token()     ((ix_token*)ix_list_get(object->parser_context.token_list, --object->parser_context.position))

#define IX_CHECK_END            ((ix_token_type)-1)
#define IX_CHECK_FALLBACK       ((ix_token_type) 0)
#define IX_CHECK_TOKEN(...)     ix_parser_check_token(object, ix_peek_token(), __VA_ARGS__)

#define IX_RETURN_IF_FAILED(condition)              \
  do {                                              \
    if (!(condition)) {                             \
      ix_delete(result);                            \
      return NULL;                                  \
    }                                               \
  } while (0)

#define IX_RETURN_IF_FAILED_EX(condition, cleanup)  \
  do {                                              \
    if (!(condition)) {                             \
      cleanup();                                    \
      return NULL;                                  \
    }                                               \
  } while (0)


#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region helper functions implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// helper functions implementation
//
//////////////////////////////////////////////////////////////////////////

static ix_operation* ix_operation_find(ix_token_type token_type)
{
  size_t i;

  for (i = 0; i < ix_count_of(ix_operation_map); i++)
  {
    if (ix_operation_map[i].token_type == token_type)
    {
      return &ix_operation_map[i];
    }
  }

  return NULL;
}

static bool ix_operation_state_exist(ix_operation_category current, ix_operation_category next)
{
  size_t i;

  for (i = 0; i < ix_count_of(ix_operation_state_pairs); i++)
  {
    if (ix_operation_state_pairs[i].current_state == current && 
        ix_operation_state_pairs[i].next_state    == next)
    {
      return true;
    }
  }

  return false;
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region ast expression ptr list functions implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// ast expression ptr list functions implementation
//
//////////////////////////////////////////////////////////////////////////

static void ix_ast_expression_ptr_list_add_node_unary(ix_ast_expression_ptr_list* operand_stack, ix_token_operation_pair* op, ix_ast_expression* e)
{
  ix_ast_expression_unary* unary = &e->type.unary;
  ix_ast_set_type(e, IXAT_EXPRESSION_UNARY);

  IX_ASSERT(operand_stack->size >= 1);

  ix_list_pop_copy(operand_stack, &unary->expression);
  unary->operation = op->token->token_type;
}

static void ix_ast_expression_ptr_list_add_node_binary(ix_ast_expression_ptr_list* operand_stack, ix_token_operation_pair* op, ix_ast_expression* e)
{
  ix_ast_expression_binary* binary = &e->type.binary;
  ix_ast_set_type(e, IXAT_EXPRESSION_BINARY);

  IX_ASSERT(operand_stack->size >= 2);

  ix_list_pop_copy(operand_stack, &binary->right);
  ix_list_pop_copy(operand_stack, &binary->left);
  binary->operation = op->token->token_type;
}

static void ix_ast_expression_ptr_list_add_node(ix_ast_expression_ptr_list* operand_stack, ix_token_operation_pair* op)
{
  ix_ast_expression* e = ix_new(ix_ast_expression);

  op->operation->category == IXTOC_UNARY_POSTFIX
    ? ix_ast_expression_ptr_list_add_node_unary(operand_stack, op, e)
    : ix_ast_expression_ptr_list_add_node_binary(operand_stack, op, e);

  ix_list_add(operand_stack, e);
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region private implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// private implementation
//
//////////////////////////////////////////////////////////////////////////

static void* ix_parser_check_token(ix_parser* object, ix_token* token, ...)
{
  typedef void* (ix_parser_callback)(ix_parser*);
  
  va_list ap;
  ix_token_type token_type;
  ix_parser_callback* parser_callback;

  //
  // function for checking incoming tokens
  //
  // varargs comes in pairs, where
  //   1st argument is: ix_token_type | IX_CHECK_FALLBACK | IX_CHECK_END
  //   2nd argument is: callback_function | NULL
  //
  // if 1st argument is ix_token_type, then "token" variable is checked among this type
  // if 1st argument is IX_CHECK_FALLBACK, then "token" variable is virtually 'checked'.
  //    this parameter must be at the very end.
  // if 1st argument is IX_CHECK_END, then this procedure ends (and 2nd argument isn't read)
  //
  // if 2nd argument is callback_function, then this callback function is called when token_type is checked
  //    or if 1st argument is IX_CHECK_FALLBACK
  // if 2nd argument is NULL, then position in parser_context is incremented
  //
  // if no token is matched, then this function throws error
  //

  va_start(ap, token);

  for (token_type = va_arg(ap, ix_token_type); token_type != IX_CHECK_END; token_type = va_arg(ap, ix_token_type))
  {
    parser_callback = va_arg(ap, ix_parser_callback*);

    if (token_type == IX_CHECK_FALLBACK || token_type == token->token_type)
    {
      void* result = parser_callback
        ? parser_callback(object)
        : ix_get_next_token();

      if (result)
      {
        return result;
      }
      else
      {
        break;
      }
    }
  }

  va_end(ap);

  //
  // if we're checking only token against no callback (callback == NULL, therefore no callback was provided),
  // and this check has failed, push an error (otherwise,
  // the callback should be responsible for pushing an error)
  // also, if no token in the list matched, push an error too
  //

  if (!parser_callback || token_type == IX_CHECK_END)
  {
    ix_error_add_ex(IXET_PARSER_UNEXPECTED_TOKEN, ix_peek_token()->source_info, ix_peek_token());
  
    //
    // try to find next expression, so we can piecefully continue
    // with parsing
    //

    do {
      IX_UNUSED(ix_get_next_token());
    } while (ix_peek_prev_token()->token_type != IXT_SEMICOLON &&
             ix_peek_prev_token()->token_type != IXT_LEFT_BRACE &&
             ix_peek_prev_token()->token_type != IXT_RIGHT_BRACE &&
             ix_peek_prev_token()->token_type != IXT_END);
  }

  return NULL;
}

static ix_ast_identifier* ix_parser_parse_identifier(ix_parser* object)
{
  ix_ast_identifier* result = ix_new(ix_ast_identifier);
  ix_ast_set_type(result, IXAT_IDENTIFIER);

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_IDENTIFIER, NULL,
    IX_CHECK_END)
  );

  strcpy(result->identifier_name, ix_peek_prev_token()->token_value);

  return result;
}

static ix_ast_expression* ix_parser_parse_expression(ix_parser* object)
{
  typedef ix_list ix_token_operation_stack;
  typedef ix_list ix_ast_expression_ptr_stack;

  ix_token_operation_stack    operation_stack;
  ix_ast_expression_ptr_stack operand_stack;
  ix_operation_category       operation_state = IXTOC_NONE;
  bool                        expression_end  = false;
  ix_ast_expression*          result          = NULL;

  //
  // implementation of shunting-yard algorithm
  //

  ix_construct_list(ix_token_operation_pair,  &operation_stack, IXLF_NONE);
  ix_construct_list(ix_ast_expression*,       &operand_stack,   IXLF_OBJECT | IXLF_POINTER);

#define ix_parse_expression_cleanup() \
  do {                                \
    ix_destroy(&operand_stack);       \
    ix_destroy(&operation_stack);     \
  } while (0);

  while (!expression_end)
  {
    ix_token* token = ix_peek_token();

    //
    // ';' or ',' ends the expression parsing
    //

    if (token->token_type == IXT_SEMICOLON ||
       (token->token_type == IXT_COMMA && ix_list_is_empty(&operation_stack)))
    {
      if (token->token_type == IXT_SEMICOLON)
      {
        IX_UNUSED(ix_get_next_token());
      }

      //
      // can we exit with this state?
      //

      IX_RETURN_IF_FAILED_EX(
        operation_state & IXTOC_TERMINAL_FLAG,
        ix_parse_expression_cleanup
      );

      break;
    }

    switch (token->token_type)
    {
      //
      // parenthesis are mixed bag
      //
      // left  parenthesis act like IDENTIFIER category, but sets next state to NONE 
      //   - there comes new expression
      // right parenthesis act like NONE category, but sets next state to IDENTIFIER
      //   - after them it's coherent whole
      //

      case IXT_LEFT_PARENTHESIS:
        IX_RETURN_IF_FAILED_EX(
          ix_operation_state_exist(operation_state, IXTOC_IDENTIFIER),
          ix_parse_expression_cleanup
        );

        {
          ix_token_operation_pair token_operation = { token, NULL };
          ix_list_add(&operation_stack, &token_operation);
          IX_UNUSED(ix_get_next_token());

          operation_state = IXTOC_NONE;
        }
        break;

      case IXT_RIGHT_PARENTHESIS:
        IX_RETURN_IF_FAILED_EX(
          ix_operation_state_exist(operation_state, IXTOC_NONE),
          ix_parse_expression_cleanup
        );

        {
          bool parenthesis_found = false;

          while (!ix_list_is_empty(&operation_stack) && !parenthesis_found)
          {
            ix_token_operation_pair token_operation;
            ix_list_pop_copy(&operation_stack, &token_operation);

            if (token_operation.token->token_type == IXT_LEFT_PARENTHESIS)
            {
              IX_UNUSED(ix_get_next_token());
              parenthesis_found = true;
            }
            else
            {
              ix_ast_expression_ptr_list_add_node(&operand_stack, &token_operation);
            }
          }

          expression_end = !parenthesis_found;

          operation_state = IXTOC_IDENTIFIER;
        }
        break;

      case IXT_IDENTIFIER:
      case IXT_CHARACTER:
      case IXT_STRING:
      case IXT_INTEGER:
      case IXT_REAL:
        IX_RETURN_IF_FAILED_EX(
          ix_operation_state_exist(operation_state, IXTOC_IDENTIFIER),
          ix_parse_expression_cleanup
        );

        {
          ix_token* next_token = ix_peek_next_token();
          ix_ast_expression* operand_expression;

          //
          // identifier + left parenthesis = function call,
          // otherwise it is value
          //
          
          operand_expression =
            token->token_type == IXT_IDENTIFIER && next_token->token_type == IXT_LEFT_PARENTHESIS
            ? ix_parser_parse_statement_call(object)
            : ix_parser_parse_expression_value(object);

          ix_list_add(&operand_stack, operand_expression);

          operation_state = IXTOC_IDENTIFIER;
        }
        break;
        
      //
      // operators
      //

      default:
        {
          ix_operation* current_operation = ix_operation_find(token->token_type);
          bool operation_state_exist = current_operation
            ? ix_operation_state_exist(operation_state, current_operation->category)
            : false;

          IX_RETURN_IF_FAILED_EX(
            current_operation && operation_state_exist,
            ix_parse_expression_cleanup
          );

          {
            ix_token_operation_pair* token_operation = ix_list_top(&operation_stack);
            ix_operation* stack_operation;

            while (!ix_list_is_empty(&operation_stack) && (stack_operation = ix_operation_find(token_operation->token->token_type)))
            {
              if ((current_operation->associativity == IXTOA_LEFT
                && current_operation->precedence == stack_operation->precedence) ||
                   current_operation->precedence <  stack_operation->precedence)
              {
                ix_ast_expression_ptr_list_add_node(&operand_stack, token_operation);

                ix_list_pop(&operation_stack);
                token_operation = ix_list_top(&operation_stack);
              }
              else
              {
                break;
              }
            }

            //
            // just dummy block to add element on stack
            //

            do {
              ix_token_operation_pair token_operation = { token, current_operation };
              ix_list_add(&operation_stack, &token_operation);
            } while (0);

            IX_UNUSED(ix_get_next_token());

            operation_state = current_operation->category;
          }
        }
        break;
    }
  }

  while (!ix_list_is_empty(&operation_stack))
  {
    ix_ast_expression_ptr_list_add_node(&operand_stack, ix_list_top(&operation_stack));
    ix_list_pop(&operation_stack);
  }

  IX_ASSERT(operand_stack.size == 1);

  ix_list_pop_copy(&operand_stack, &result);

  ix_parse_expression_cleanup();

#undef ix_parse_expression_cleanup

  return result;
}

static ix_ast_expression* ix_parser_parse_expression_value(ix_parser* object)
{
  return IX_CHECK_TOKEN(
    IXT_IDENTIFIER, ix_parser_parse_expression_value_variable,
    IXT_STRING,     ix_parser_parse_expression_value_string,
    IXT_INTEGER,    ix_parser_parse_expression_value_number_integer,
    IXT_REAL,       ix_parser_parse_expression_value_number_real,
  IX_CHECK_END);
}

static ix_ast_expression* ix_parser_parse_expression_value_variable(ix_parser* object)
{
  ix_ast_expression* result = ix_new(ix_ast_expression);
  ix_ast_value* value = &result->type.value;
  ix_ast_set_type(result, IXAT_VALUE_VARIABLE);

  value->variable = ix_parser_parse_identifier(object);

  IX_RETURN_IF_FAILED(
    value->variable
  );

  return result;
}

static ix_ast_expression* ix_parser_parse_expression_value_string(ix_parser* object)
{
  ix_ast_expression* result = ix_new(ix_ast_expression);
  ix_ast_value* value = &result->type.value;
  ix_ast_set_type(result, IXAT_VALUE_STRING);

  ix_construct(ix_string, &value->string);
  ix_string_set_array(&value->string, ix_peek_token()->token_value, strlen(ix_peek_token()->token_value));
  IX_UNUSED(ix_get_next_token());

  return result;
}

static ix_ast_expression* ix_parser_parse_expression_value_number_integer(ix_parser* object)
{
  ix_ast_expression* result = ix_new(ix_ast_expression);
  ix_ast_value* value = &result->type.value;
  ix_ast_set_type(result, IXAT_VALUE_INTEGER);

  value->integer = atoll(ix_peek_token()->token_value);
  IX_UNUSED(ix_get_next_token());

  return result;
}

static ix_ast_expression* ix_parser_parse_expression_value_number_real(ix_parser* object)
{
  ix_ast_expression* result = ix_new(ix_ast_expression);
  ix_ast_value* value = &result->type.value;
  ix_ast_set_type(result, IXAT_VALUE_REAL);

  value->real = atof(ix_peek_token()->token_value);
  IX_UNUSED(ix_get_next_token());

  return result;
}

static ix_ast_expression* ix_parser_parse_statement_block(ix_parser* object)
{
  bool one_expression_statement = false;
  bool error_flag = false;

  ix_ast_expression* result = ix_new(ix_ast_expression);
  ix_ast_statement_block* block = &result->type.statement.st_block;
  ix_ast_set_type(result, IXAT_STATEMENT_BLOCK);

  block->statement_list = ix_new_list(ix_ast_expression*, IXLF_OBJECT | IXLF_POINTER);

  //
  // block := { expression* }
  //

  if (!(one_expression_statement = (ix_peek_token()->token_type != IXT_LEFT_BRACE)))
  {
    //
    // skip left brace, if present
    //

    IX_UNUSED(ix_get_next_token());
  }

  do
  {
    ix_ast_expression* expression;

    //
    // allow syntax like:
    // i = 5 ;;;;;;;;; i = 6 ;;;
    //

    if (ix_peek_token()->token_type == IXT_SEMICOLON)
    {
      IX_UNUSED(ix_get_next_token());
      continue;
    }

    expression = IX_CHECK_TOKEN(
      IXT_IDENTIFIER,   ix_parser_parse_expression,
      IXT_KW_FUNCTION,  ix_parser_parse_statement_function,
      IXT_KW_VAR,       ix_parser_parse_statement_declaration,
      IXT_KW_FOR,       ix_parser_parse_statement_for,
      IXT_KW_WHILE,     ix_parser_parse_statement_while,
      IXT_KW_DO,        ix_parser_parse_statement_do_while,
      IXT_KW_IF,        ix_parser_parse_statement_if,
      IXT_KW_CONTINUE,  ix_parser_parse_statement_last,
      IXT_KW_BREAK,     ix_parser_parse_statement_last,
      IXT_KW_RETURN,    ix_parser_parse_statement_last,
    IX_CHECK_END);

    error_flag |= !expression;

    if (expression)
    {
      ix_list_add(block->statement_list, expression);
    }
  } while (

    //
    // end immediately, if we're in one line expression
    // end on right brace, if we're in block
    // also end when we reach end somehow
    //

    !one_expression_statement &&
      (ix_peek_token()->token_type != IXT_RIGHT_BRACE || one_expression_statement) &&
    ix_peek_token()->token_type != IXT_END
  );

  if (!one_expression_statement)
  {
    error_flag |= !IX_CHECK_TOKEN(
      IXT_RIGHT_BRACE, NULL,
    IX_CHECK_END);
  }

  if (error_flag)
  {
    ix_delete(result);
    result = NULL;
  }

  return result;
}

static ix_ast_expression* ix_parser_parse_statement_for(ix_parser* object)
{
  bool error_flag = false;

  ix_ast_expression* result = ix_new(ix_ast_expression);
  ix_ast_statement_for* for_loop = &result->type.statement.st_for;
  ix_ast_set_type(result, IXAT_STATEMENT_FOR);

  //
  // for := for (init; condition; count) { block }
  //

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_KW_FOR, NULL,
    IX_CHECK_END)
  );

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_LEFT_PARENTHESIS, NULL,
    IX_CHECK_END)
  );

  //
  // init = declaration | expression
  //

  for_loop->init = IX_CHECK_TOKEN(
    IXT_KW_VAR,        ix_parser_parse_statement_declaration,
    IX_CHECK_FALLBACK, ix_parser_parse_expression
  );

  //
  // don't check anything here even if error was detected,
  // since check function moved us onto begin of new expression
  //

  for_loop->condition = ix_parser_parse_expression(object);
  for_loop->count     = ix_parser_parse_expression(object);

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_RIGHT_PARENTHESIS, NULL,
    IX_CHECK_END)
  );

  for_loop->block     = ix_parser_parse_statement_block(object);

  error_flag |= !for_loop->init && for_loop->condition && !for_loop->count && !for_loop->block;

  IX_RETURN_IF_FAILED(
    !error_flag
  );

  return result;
}

static ix_ast_expression* ix_parser_parse_statement_while(ix_parser* object)
{
  bool error_flag = false;

  ix_ast_expression* result = ix_new(ix_ast_expression);
  ix_ast_statement_while* while_loop = &result->type.statement.st_while;
  ix_ast_set_type(result, IXAT_STATEMENT_WHILE);

  //
  // while := while (condition) { block }
  //

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_KW_WHILE, NULL,
    IX_CHECK_END)
  );

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_LEFT_PARENTHESIS, NULL,
    IX_CHECK_END)
  );

  while_loop->condition = ix_parser_parse_expression(object);

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_RIGHT_PARENTHESIS, NULL,
    IX_CHECK_END)
  );

  while_loop->block     = ix_parser_parse_statement_block(object);

  error_flag |= while_loop->condition && !while_loop->block;

  IX_RETURN_IF_FAILED(
    !error_flag
  );

  return result;
}

static ix_ast_expression* ix_parser_parse_statement_do_while(ix_parser* object)
{
  bool error_flag = false;

  ix_ast_expression* result = ix_new(ix_ast_expression);
  ix_ast_statement_do_while* do_while_loop = &result->type.statement.st_do_while;
  ix_ast_set_type(result, IXAT_STATEMENT_DO_WHILE);

  //
  // do_while := do { block } while (condition)
  //

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_KW_DO, NULL,
    IX_CHECK_END)
  );

  do_while_loop->block = ix_parser_parse_statement_block(object);

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_KW_WHILE, NULL,
    IX_CHECK_END)
  );

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_LEFT_PARENTHESIS, NULL,
    IX_CHECK_END)
  );

  do_while_loop->condition = ix_parser_parse_expression(object);

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_RIGHT_PARENTHESIS, NULL,
    IX_CHECK_END)
  );

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_SEMICOLON, NULL,
    IX_CHECK_END)
  );

  error_flag |= !do_while_loop->block && !do_while_loop->condition;

  IX_RETURN_IF_FAILED(
    !error_flag
  );

  return result;
}

static ix_ast_expression* ix_parser_parse_statement_if(ix_parser* object)
{
  bool error_flag = false;

  ix_ast_expression* result = ix_new(ix_ast_expression);
  ix_ast_statement_if* if_condition = &result->type.statement.st_if;
  ix_ast_set_type(result, IXAT_STATEMENT_IF);

  //
  // if := if (condition) { true_block } [ else { false_block } ]
  //

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_KW_IF, NULL,
    IX_CHECK_END)
  );

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_LEFT_PARENTHESIS, NULL,
    IX_CHECK_END)
  );

  if_condition->condition = ix_parser_parse_expression(object);

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_RIGHT_PARENTHESIS, NULL,
    IX_CHECK_END)
  );

  if_condition->true_block = ix_parser_parse_statement_block(object);

  error_flag |= !if_condition->condition && !if_condition->true_block;

  IX_RETURN_IF_FAILED(
    !error_flag
  );

  if (ix_peek_token()->token_type == IXT_KW_ELSE)
  {
    IX_UNUSED(ix_get_next_token());

    if_condition->false_block = ix_parser_parse_statement_block(object);

    error_flag |= !if_condition->false_block;

    IX_RETURN_IF_FAILED(
      !error_flag
    );
  }

  return result;
}

static ix_ast_expression* ix_parser_parse_statement_last(ix_parser* object)
{
  ix_ast_expression* result = ix_new(ix_ast_expression);
  ix_ast_statement_last* last = &result->type.statement.st_last;
  ix_ast_set_type(result, IXAT_STATEMENT_LAST);

  //
  // last := continue | break | return [ expression ]
  //

  last->keyword = ix_peek_token()->token_type;

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_KW_CONTINUE,  NULL,
      IXT_KW_BREAK,     NULL,
      IXT_KW_RETURN,    NULL,
    IX_CHECK_END)
  );

  if (last->keyword == IXT_KW_RETURN && ix_peek_token()->token_type != IXT_SEMICOLON)
  {
    last->expression = ix_parser_parse_expression(object);

    IX_RETURN_IF_FAILED(
      last->expression
    );
  }
  else
  {
    //
    // skip over semicolon
    //

    IX_RETURN_IF_FAILED(
      IX_CHECK_TOKEN(
        IXT_SEMICOLON, NULL,
      IX_CHECK_END)
    );
  }

  return result;
}

static ix_ast_expression* ix_parser_parse_statement_function(ix_parser* object)
{
  ix_ast_expression* result = ix_new(ix_ast_expression);
  ix_ast_statement_function* function = &result->type.statement.st_function;
  ix_ast_set_type(result, IXAT_STATEMENT_FUNCTION);

  //
  // function := function_name(parameter_list) { block }
  //

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_KW_FUNCTION, NULL,
    IX_CHECK_END)
  );

  function->function_name = ix_parser_parse_identifier(object);
  
  IX_RETURN_IF_FAILED(
    function->function_name
  );

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_LEFT_PARENTHESIS, NULL,
    IX_CHECK_END)
  );

  function->parameter_list = ix_new_list(ix_ast_identifier*, IXLF_OBJECT | IXLF_POINTER);

  for (;;)
  {
    ix_token* token = ix_peek_token();
    ix_ast_identifier* identifier;

    identifier = IX_CHECK_TOKEN(
        IXT_RIGHT_PARENTHESIS, NULL,
        IXT_COMMA, NULL,
        IXT_IDENTIFIER, ix_parser_parse_identifier,
    IX_CHECK_END);

    IX_RETURN_IF_FAILED(
      identifier
    );

    if (token->token_type == IXT_RIGHT_PARENTHESIS)
    {
      break;
    }

    if (token->token_type == IXT_COMMA)
    {
      continue;
    }

    ix_list_add(function->parameter_list, identifier);
  }

  function->body = ix_parser_parse_statement_block(object);

  IX_RETURN_IF_FAILED(
    function->body
  );

  return result;
}

static ix_ast_expression* ix_parser_parse_statement_call(ix_parser* object)
{
  ix_ast_expression* result = ix_new(ix_ast_expression);
  ix_ast_statement_call* call = &result->type.statement.st_call;
  ix_ast_set_type(result, IXAT_STATEMENT_CALL);

  //
  // call := function_name(argument_list)
  //

  call->function_name = ix_parser_parse_identifier(object);

  IX_CHECK_TOKEN(
    IXT_LEFT_PARENTHESIS, NULL,
  IX_CHECK_END);

  call->argument_list = ix_new_list(ix_ast_expression*, IXLF_OBJECT | IXLF_POINTER);

  for (;;)
  {
    ix_token* token = ix_peek_token();
    ix_ast_expression* expression;

    expression = IX_CHECK_TOKEN(
      IXT_RIGHT_PARENTHESIS,  NULL,
      IXT_COMMA,              NULL,
      IX_CHECK_FALLBACK,      ix_parser_parse_expression
    );

    IX_RETURN_IF_FAILED(
      expression
    );

    if (token->token_type == IXT_RIGHT_PARENTHESIS)
    {
      break;
    }

    if (token->token_type == IXT_COMMA)
    {
      continue;
    }

    ix_list_add(call->argument_list, expression);
  }

  return result;
}

static ix_ast_expression* ix_parser_parse_statement_declaration(ix_parser* object)
{
  ix_ast_expression* result = ix_new(ix_ast_expression);
  ix_ast_statement_declaration* declaration = &result->type.statement.st_declaration;
  ix_ast_set_type(result, IXAT_STATEMENT_DECLARATION);

  //
  // declaration := var identifier [ = expression ];
  //

  IX_RETURN_IF_FAILED(
    IX_CHECK_TOKEN(
      IXT_KW_VAR, NULL,
    IX_CHECK_END)
  );

  declaration->identifier = ix_parser_parse_identifier(object);
  
  IX_RETURN_IF_FAILED(
    declaration->identifier
  );

  //
  // check for = or ;
  //

  if (ix_peek_token()->token_type == IXT_ASSIGN)
  {
    IX_UNUSED(ix_get_prev_token());
    declaration->expression = ix_parser_parse_expression(object);

    IX_RETURN_IF_FAILED(
      declaration->expression
    );
  }
  else
  {
    if (ix_peek_token()->token_type == IXT_SEMICOLON)
    {
      IX_UNUSED(ix_get_next_token());
    }
    else
    {
      //
      // just fail here
      //

      IX_RETURN_IF_FAILED(
        true
      );
    }
  }

  return result;
}

static ix_ast_program* ix_parser_parse_program(ix_parser* object)
{
  //
  // first token should be IXT_BEGIN, skip it
  //

  IX_UNUSED(ix_get_next_token());

  //
  // ...but check it for IXT_BEGIN type,
  // if it's not there, throw internal error
  //

  if (ix_peek_prev_token()->token_type == IXT_BEGIN)
  {
    bool error_flag = false;

    ix_ast_program* result = ix_new(ix_ast_program);
    result->statement_list = ix_new_list(ix_ast_expression*, IXLF_OBJECT | IXLF_POINTER);

    while (ix_peek_token()->token_type != IXT_END)
    {
      ix_ast_expression* statement = ix_parser_parse_statement_block(object);

      error_flag |= !statement || (object->errors->size > 0);

      if (statement)
      {
        ix_list_add(result->statement_list, statement);
      }
    }

    if (error_flag)
    {
      ix_delete(result);
      ix_throw();
    }

    return result;
  }
  else
  {
    ix_error_add(IXET_PARSER_INTERNAL_ERROR, ix_peek_token()->source_info);
  }

  //
  // since INTERNAL_ERROR is fatal, we should never reach this point
  //

  return NULL;
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix_parser* ix_parser_ctor(ix_parser* object, ix_error_list* errors)
{
  IX_OBJECT_SET_DTOR(object, &ix_parser_dtor);

  object->parser_context.token_list = NULL;
  object->parser_context.position = 0;

  object->errors = errors;

  return object;
}

void ix_parser_dtor(ix_object* object)
{
  ix_parser* parser = (ix_parser*)object;

  IX_UNUSED(parser);
}

void ix_parser_parse_token_list(ix_parser* object, ix_token_list* token_list, ix_ast* ast)
{
  //
  // entry point of the parser
  //

  object->parser_context.token_list = token_list;

  ast->program = ix_parser_parse_program(object);
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
