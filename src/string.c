#include "string.h"

#include <string.h>

#ifdef IX_COMPILER_MSVC
# pragma region public implementation
#endif

//////////////////////////////////////////////////////////////////////////
//
// public implementation
//
//////////////////////////////////////////////////////////////////////////

ix_string* ix_string_ctor(ix_string* object)
{
  IX_OBJECT_SET_DTOR(object, &ix_string_dtor);

  object->content = ix_alloc_array(char, IX_STRING_DEFAULT_CAPACITY);
  object->size = 0;
  object->capacity = IX_STRING_DEFAULT_CAPACITY;

  object->content[0] = '\0';

  return object;
}

void ix_string_dtor(ix_object* object)
{
  ix_string* string = (ix_string*)object;

  ix_free(string->content);
  string->content = NULL;
  string->size = 0;
  string->capacity = 0;
}

void ix_string_copy_from(ix_string* object, ix_string* source)
{
  if (object->content)
  {
    ix_destroy(object);
  }

  object->capacity = source->capacity;
  object->content = ix_alloc_array(char, source->capacity);
  object->size = source->size;

  strcpy(object->content, source->content);
}

void ix_string_reserve(ix_string* object, size_t size)
{
  if (object->capacity < (size + 1))
  {
    object->content = realloc(object->content, object->capacity = (size + 1));
  }
}

void ix_string_append_array(ix_string* object, const char* string, size_t size)
{
  while ((object->size + size + 1) >= object->capacity)
  {
    object->content = realloc(object->content, object->capacity <<= 2);
  }

  memcpy(&object->content[object->size], string, size);
  object->size += size;
  object->content[object->size] = '\0';
}

void ix_string_set_array(ix_string* object, const char* string, size_t size)
{
  while ((object->size + size + 1) >= object->capacity)
  {
    object->content = realloc(object->content, object->capacity <<= 2);
  }

  memcpy(object->content, string, size);
  object->size = size;
  object->content[object->size] = '\0';
}

void ix_string_append(ix_string* object, const char* string)
{
  ix_string_append_array(object, string, strlen(string));
}

void ix_string_append_char(ix_string* object, char c)
{
  ix_string_append_array(object, &c, 1);
}

void ix_string_append_string(ix_string* object, ix_string* string)
{
  ix_string_append_array(object, string->content, string->size);
}

void ix_string_set(ix_string* object, const char* string)
{
  ix_string_set_array(object, string, strlen(string));
}

void ix_string_set_char(ix_string* object, char c)
{
  ix_string_set_array(object, &c, 1);
}

void ix_string_set_string(ix_string* object, ix_string* string)
{
  ix_string_set_array(object, string->content, string->size);
}

void ix_string_substring(ix_string* object, ix_string* dest, size_t index, size_t count)
{
  ix_string_set_array(dest, object->content + index, count);
}

uint32_t ix_string_hash(const char* string)
{
  uint32_t result, i;
  size_t len = strlen(string);

  for (result = i = 0; i < len; ++i)
  {
    result += string[i];
    result += (result << 10);
    result ^= (result >> 6);
  }

  result += (result << 3);
  result ^= (result >> 11);
  result += (result << 15);

  return result;
}

#ifdef IX_COMPILER_MSVC
# pragma endregion
#endif
