#pragma once
#ifndef IX_FILE_STREAM_H
#define IX_FILE_STREAM_H

#include "stream.h"

#include <stdio.h>

typedef struct ix_tag_file_stream
{
  ix_stream base;

  bool (*open)(void* object, const char* filename, const char* mode);

  FILE* file;
} ix_file_stream;

ix_file_stream* ix_file_stream_ctor(ix_file_stream* object);
void            ix_file_stream_dtor(ix_object* object);

bool            ix_file_stream_open(void* object, const char* filename, const char* mode);

#endif
